#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from externals.colors import rainbow
import lightprotocol
import lightfunction
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import model
from multiprocess.pool import Pool
import numpy as np
import parameters
import simulate
from time import time
start = time()

__author__ = "Philipp Norf"
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"

# Initialising model and setting simulation parameters
p = parameters.Parameters(swATPsynthase=-2)
intervals = np.arange(15, 451, 15)
y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0.0001, 0.0000, 0.0001, 1, 0.0000])  # y0 as in the public model
# light model:     P       C      F       A     Eact      N       H   anT    Q


def simulation(interval):
    # return 1, 4, 1, 8, 3, 8, 2, 4, 6
    m = model.LightModel(p)
    l = lightprotocol.LightProtocol(length=4000, pfd_start=0, pfd_later=0, dt_pulse=0.6, pfd_pulse=5000)
    l.create(pam='0_' + str(interval))
    lfn = lightfunction.LightFunction(l, '_constant')
    s = simulate.Simulate(m, lfn, y0)
    s.piecewise_constant()
    s.finish()
    return s.get_fluo()

pool = Pool()
results = pool.map(simulation, intervals)

print('elapsed time: ', time() - start)

i = 0
colour = rainbow(len(results) + 1)  # + 1 to avoid yellow as line colour
f = plt.figure()
for T, F, Fmax, Tm, Fm, Ts, Fs, Bst, CS2 in results:
    c = next(colour)
    plt.plot(Tm, Fm/Fmax, '-', color=c, label=str(intervals[i])+' s')
    # plt.plot(Ts, Fs/Fmax, '.:', color=c)
    i += 1

plt.xlim([0, 3600])
plt.xlabel('Time [s]')
plt.ylabel('Normalised Fluorescence')

# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

# Saving both plots to single PDF
# pp = PdfPages('../latex/graphics/insilico_interference_test.pdf')
pp = PdfPages('insilico_interference_test.pdf')
pp.savefig(f)
pp.close()

plt.show()
