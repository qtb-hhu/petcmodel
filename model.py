#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from abc import ABCMeta, abstractmethod
from enum import IntEnum
from miscellaneous import ModelError, pH, pHinv, Switch
import numpy as np
from numpy.core.umath import exp

__author__ = ["Anna Matuszyńska", "Philipp Norf"]
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class Rates:
    def __init__(self, parameters):
        """ returns set of parameters """
        self.par = parameters

    # -------------------------------------------- #
    # methods required to calculate reaction rates #

    def ps2states(self, Pox, Q, L):
        """ QSSA, calculates the states of the photosystem II
            accepts values:
            Pox: oxidised fraction of the PQ pool (PQH2)
            Q: quencher
            L: light, int or array of the n x 1 dimension, that gives the light intensity

            returns:
            B: array of arrays with the states of PSII; rows: time, columns states: 1 and 3 excited
        """
        Pred = self.par.PQtot - Pox

        k2 = self.par.k2
        kF = self.par.kF
        kH = self.par.kH0 + self.par.kH * Q

        k3p = self.par.kPQred * Pox
        k3m = self.par.kPQred * Pred / self.par.KeqPQred

        M = np.array([[-L-k3m, kH+kF,          k3p, 0],
                      [L,      -(kH+kF+k2),    0,   0],
                      [0,      0,              L,   -(kH+kF)],
                      [1,      1,              1,   1]])

        A = np.array([0, 0, 0, self.par.PSIItot])
        B = np.linalg.solve(M, A)

        return B

    def ps1states(self, C, F, L):
        """ under QSSA method calculates open state of PSI
            depends on reduction states of plastocyanin and ferredoxin
            C = [PC], F = [Fd] (ox. forms)
            accepts: light, y as an array of arrays
            returns: array of PSI open
        """
        A1 = self.par.PSItot / \
        (1 + L/(self.par.kFdred * F) +
        (1 + (self.par.Fdtot - F)/(self.par.KeqFAFd * F)) * (C/(self.par.KeqPCP700 * (self.par.PCtot - C)) +
        L/(self.par.kPCox * (self.par.PCtot-C)))
        )

        return A1

    # ---------------------------------- #
    # method to calculate cross sections #

    def crossSectionSimple(self, Ant):
        """ calculates the cross section of PSII """
        cs = self.par.staticAntII + (1 - self.par.staticAntII - self.par.staticAntI) * Ant
        return cs

    # --------------------------- #
    # 14 reaction rates separately #

    def vPS2(self, Pox, H, vQ, LII):
        """ reaction rate constant for photochemistry """
        B = self.ps2states(Pox, vQ, LII)
        v = self.par.k2 * B[1] / 2
        return v

    def vPS1(self, C, F, LI):
        """ reaction rate constant for open PSI """
        A = self.ps1states(C, F, LI)
        v = LI * A
        return v

    def vPTOX(self, Pox):
        """ calculates reaction rate of PTOX """
        v = (self.par.PQtot - Pox) * self.par.kPTOX * self.par.O2ext
        return v

    def vB6f(self, Pox, C, H):
        """ calculates reaction rate of cytb6f """
        ph = pH(H)
        Keq = self.Keq_cytb6f(ph)

        v = max(self.par.kCytb6f * ((self.par.PQtot - Pox) * C**2 - (Pox * (self.par.PCtot - C)**2)/Keq),
                -self.par.kCytb6f)
        return v

    def vNDH(self, Pox):
        """ calculates reaction rate of PQ reduction under absence of oxygen
            can be mediated by NADH reductase NDH
        """
        v = self.par.kNDH * Pox
        return v

    def vCyc(self, Pox, F):
        """ calculates reaction rate of cyclic electron flow
            considered as practically irreversible
        """
        v = self.par.kcyc * (((self.par.Fdtot - F)**2) * Pox)
        return v

    def vFNR(self, F,N):
        """ reaction rate mediated by FNR
            uses convenience kinetics
        """
        fdred = (self.par.Fdtot - F)/self.par.KM_FNR_F
        fdox = F/self.par.KM_FNR_F
        nadph = N/self.par.KM_FNR_N
        nadp = (self.par.NADPtot - N)/self.par.KM_FNR_N

        v = (self.par.EFNR * self.par.kcatFNR *
            ((fdred**2) * nadp - ((fdox**2) * nadph) / self.par.KeqFNR) /
            ((1 + fdred + fdred**2) * (1 + nadp) + (1 + fdox + fdox**2) * (1 + nadph) - 1))
        return v

    def vLeak(self, Hlf):
        """ rate of leak of protons through the membrane """
        v = self.par.kLeak * (Hlf - pHinv(self.par.pHstroma))
        return v

    def vSt12(self, Pox,Ant):
        """ reaction rate of state transitions from PSII to PSI
            Ant depending on module used corresponds to non-phosphorylated antennae
            or antennae associated with PSII
        """
        kKin = self.par.kStt7 * (1 / (1 + ((Pox / self.par.PQtot) / self.par.KM_ST)**self.par.n_ST))
        v = kKin * Ant
        return v

    def vSt21(self, Ant):
        """ reaction rate of state transitions from PSI to PSII """
        v = self.par.kPph1 * (1 - Ant)
        return v

    # ---------------------- #
    # rates of main products #

    def vATPsynthase(self, A, Hlf, Eact):
        ph = pH(Hlf)
        ADP = self.par.APtot - A
        v = Eact * self.par.kATPsynth * (ADP - A / self.Keq_ATP(ph))
        return v

    def vATPactivity(self, PFD, Eact):
        """
        Returns the activity of the ATPsynthase in response to light.

        Parameters
        ----------
        PFD: float
            Amount of light as photon flux density.

        Eact: float
            current state of the enzymes activity.

        Returns
        -------
        number
            Calculated activity of the enzyme.

        """
        switch = PFD >= 1  # sf2(PFD)
        vf = self.par.kActATPase * switch * (1 - Eact)  # assumes ATPsynthase total = 1
        vr = self.par.kDeactATPase * (1 - switch) * Eact
        return vf - vr

    def vATPconsumption(self, A):
        v = self.par.kATPcons * A
        return v

    def vNADPHconsumption(self, N):
        v = self.par.kNADPHcons * N
        return v

    # ---------------------------------------------- #
    # model (parameter) dependent helper functions   #

    def Keq_ATP(self, pH):
        DG = self.par.DeltaG0_ATP - self.par.dG_pH * self.par.HPR * (self.par.pHstroma - pH)
        Keq = self.par.Pi_mol * exp(-DG/self.par.RT)
        return Keq

    def Keq_cytb6f(self, pH):
        DG1 = -2 * self.par.F * self.par.E0_PQ
        DG2 = -self.par.F * self.par.E0_PC

        DG = - (DG1 + 2 * self.par.dG_pH * pH) + 2 * DG2 + 2 * self.par.dG_pH * (self.par.pHstroma - pH)
        Keq = exp(-DG/self.par.RT)
        return Keq

    def vdeep(self, Q, H):
        kH_act = H**self.par.nH / (H**self.par.nH + pHinv(self.par.pHdeep)**self.par.nH)
        return self.par.kdeep * (1-Q) * kH_act

    def vepox(self, Q, H):
        # Redundant parameter H allows DiatomModel to inherit LightModel.rates rather than overwriting it for one glyph.
        return self.par.kepox * Q


class _StateVariableIndices:
    """ Enumeration. Stores the order of the starting values within model object."""
    def __init__(self, statevariables):
        self._name = IntEnum('StateVariables', zip(statevariables, range(len(statevariables))))

    def __getattr__(self, attr):
        """
        Allows to access the state variable enumeration directly as attributes of LightModel.

        Parameters
        ----------
        attr: str
            Name of an attribute.

        Returns
        -------
        IntEnum
            State variable enumeration.
        """
        try:
            return self._name[attr]
        except AttributeError:
            raise AttributeError("type object 'LightModel' has no attribute '{}'".format(attr))


# The several classes may seem redundant but this way the original model stays in place. The only change made forces the
# equation for the ATP synthase rate to ignore the light activation.
class Model(Rates, _StateVariableIndices):
    __metaclass__ = ABCMeta

    def __init__(self, parameters, statevariablenames):
        Rates.__init__(self, parameters)
        _StateVariableIndices.__init__(self, statevariablenames)
        print('You run simulations based on the model published in 2014 by Ebenhoeh et al.')

    def __call__(self, y, PFD):
        return self.model(y, PFD)

    @abstractmethod
    def rates(self, y, PFD):
        raise NotImplementedError("Abstract base class methods must be overwritten by child classes!")

    @abstractmethod
    def model(self, y, PFD):
        raise NotImplementedError("Abstract base class methods must be overwritten by child classes!")


class PETCModel(Model):
    """
    This model is based on the PETC model from the original Publication by Ebenhöh et al. 2014
    This is called OldModel in Anna's lightmodel project.

    Updated on 28 Oct 2014: recreates PETCmodel from publication
    Updated on 24 Nov 2016: Adds simple light regulation for ATP synthase
                            swATPsynthase parameter. States: on (1), off (0), regulated (-1)
    """
    def __init__(self, parameters):
        super().__init__(parameters, ['P', 'C', 'F', 'A', 'N', 'H', 'T', 'Q'])

    def rates(self, y, PFD):
        """
        Method calculating reaction rates.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, E, N, H, T, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            concentration of NADPH (N), lumenal protons (H), non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        dict
            A dictionary with the rate names as keys and the rates as values.
        """
        P, C, F, A, N, H, T, Q = y

        cs2 = self.crossSectionSimple(T)
        LII = cs2 * PFD
        LI = (1-cs2) * PFD  # assumption: total cross section is always one
        Eact = ((self.par.swATPsynthase == -1) and (PFD >= 1)) or self.par.swATPsynthase > 0
        vdeep = self.vdeep(Q, H)
        vepox = self.vepox(Q, H)

        v = {
            'ps2': self.vPS2(P, H, vdeep - vepox, LII),
            'ps1': self.vPS1(C, F, LI),
            'ptox': self.vPTOX(P),
            'cytb6f': self.vB6f(P, C, H),
            'ndh': self.vNDH(P),
            'fnr': self.vFNR(F, N),
            'cyc': self.vCyc(P, F),
            'ATPsynthase': self.vATPsynthase(A, H, Eact),
            'ATPconsumption': self.vATPconsumption(A),
            'NADPHconsumption': self.vNADPHconsumption(N),
            'leak': self.vLeak(H),
            'st12': self.vSt12(P, T),
            'st21': self.vSt21(T),
            'deep': vdeep,
            'epox': vepox,
            'cs2': cs2,
            }

        return v

    def model(self, y, PFD):
        """
        Defining the system of eight equations governing the evolution of the photosynthetic electron transport chain.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, E, N, H, T, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            concentration of NADPH (N), lumenal protons (H), non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        np.array
            An array containing the value of y for each desired time in t, with the initial value `y0` in the first row.
        """
        # raise error if numerical problems occur regarding excess protonation
        if y[self.H] <= 0:
            raise ModelError("H went below zero")

        # Reaction rates, v, is a dictionary
        v = self.rates(y, PFD)

        # Output from ODE function must be a COLUMN vector, with n rows
        dydt = np.zeros([8, 1])

        # dP/dt = -vPSII + vCytb6f - vcyc + vPTOX - vNDH
        dydt[0] = - v['ps2'] + v['cytb6f'] - v['cyc'] + v['ptox'] - v['ndh']

        # dC/dt = -2vCytb6f + vPSI
        dydt[1] = -2 * v['cytb6f'] + v['ps1']

        # dF/dt = -vPSI + 2vFNR + 2vcyc
        dydt[2] = - v['ps1'] + 2 * v['fnr'] + 2 * v['cyc']

        # dATP/dt = vATPsynthase - vATPconsumption
        dydt[3] = v['ATPsynthase'] - v['ATPconsumption']

        # dN/dt = vFNR - vNADPHconsumption
        dydt[4] = v['fnr'] - v['NADPHconsumption']

        # dH/dt = water_split + proton_pump - synthase_ATP - leak
        dydt[5] = (2 * v['ps2'] + 4 * v['cytb6f'] - self.par.HPR * v['ATPsynthase'] - v['leak']) / self.par.bH

        # dT/dt =  non-phosphorylated antennae // antenna associated with PSII
        dydt[6] = - v['st12'] + v['st21']

        # original MATLAB model with simpel quencher
        dydt[7] = v['deep'] - v['epox']

        return dydt


class LightModel(Model):
    """
    This model is based on the PETC model from the original Publication by Ebenhöh et al. 2014
    This is called OldModel in Anna's lightmodel project.

    Updated on 24 Nov 2016: Adds light regulation for ATP synthase.
    """
    def __init__(self, parameters):
        super().__init__(parameters, ['P', 'C', 'F', 'A', 'E', 'N', 'H', 'T', 'Q'])

    def rates(self, y, PFD):
        """
        Method calculating reaction rates.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, E, N, H, T, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            ATPsynthase activity (E), concentration of NADPH (N), lumenal protons (H),
            non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        dict
            A dictionary with the rate names as keys and the rates as values.
        """
        P, C, F, A, E, N, H, T, Q = y

        cs2 = self.crossSectionSimple(T)
        LII = cs2 * PFD
        LI = (1-cs2) * PFD  # assumption: total cross section is always one
        Eact = {1: 1, 0: 0, -1: PFD >= 1, -2: E}[self.par.swATPsynthase]
        vdeep = self.vdeep(Q, H)
        vepox = self.vepox(Q, H)

        v = {
            'ps2': self.vPS2(P, H, vdeep - vepox, LII),
            'ps1': self.vPS1(C, F, LI),
            'ptox': self.vPTOX(P),
            'cytb6f': self.vB6f(P, C, H),
            'ndh': self.vNDH(P),
            'fnr': self.vFNR(F, N),
            'cyc': self.vCyc(P, F),
            'ATPsynthase': self.vATPsynthase(A, H, Eact),
            'ATPconsumption': self.vATPconsumption(A),
            'NADPHconsumption': self.vNADPHconsumption(N),
            'leak': self.vLeak(H),
            'st12': self.vSt12(P, T),
            'st21': self.vSt21(T),
            'deep': vdeep,
            'epox': vepox,
            'cs2': cs2,
            'ATPactivity': self.vATPactivity(PFD, E)
            }

        return v

    def model(self, y, PFD):
        """
        Defining the system of nine equations governing the evolution of the photosynthetic electron transport chain.

        ATPsynthase activation enzyme is light activated.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, Eact, N, H, anT, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            ATPsynthase activity (E), concentration of NADPH (N), lumenal protons (H),
            non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        np.array
            An array containing the value of y for each desired time in t, with the initial value `y0` in the first row.
        """
        # raise error if numerical problems occur regarding excess protonation
        if y[self.H] <= 0:
            raise ModelError("H went below zero")

        # Reaction rates, v, is a dictionary
        v = self.rates(y, PFD)

        # Output from ODE function must be a COLUMN vector, with n rows
        dydt = np.zeros([9, 1])

        # dP/dt = -vPSII + vCytb6f - vcyc + vPTOX - vNDH
        dydt[0] = - v['ps2'] + v['cytb6f'] - v['cyc'] + v['ptox'] - v['ndh']

        # dC/dt = -2vCytb6f + vPSI
        dydt[1] = -2 * v['cytb6f'] + v['ps1']

        # dF/dt = -vPSI + 2vFNR + 2vcyc
        dydt[2] = - v['ps1'] + 2 * v['fnr'] + 2 * v['cyc']

        # dATP/dt = vATPsynthase - vATPconsumption
        dydt[3] = v['ATPsynthase'] - v['ATPconsumption']

        # dATPactivity/dt = vATPactivitiy
        dydt[4] = v['ATPactivity']

        # dN/dt = vFNR - vNADPHconsumption
        dydt[5] = v['fnr'] - v['NADPHconsumption']

        # dH/dt = water_split + proton_pump - synthase_ATP - leak
        dydt[6] = (2 * v['ps2'] + 4 * v['cytb6f'] - self.par.HPR * v['ATPsynthase'] - v['leak']) / self.par.bH

        # dT/dt =  non-phosphorylated antennae // antenna associated with PSII
        dydt[7] = - v['st12'] + v['st21']

        # original MATLAB model with simple quencher
        dydt[8] = v['deep'] - v['epox']

        return dydt


class DiatomModel(Model):
    """
    This model is based on the PETC model from the original Publication by Ebenhöh et al. 2014

    Updated on 24 Nov 2016: Adds light regulation for ATP synthase.
    Updated on 25 Nov 2016: Diatom quencher uses a proton feedback loop.
    Updated on 27 Nov 2016: Diatom quencher uses a Dt feedback loop.
    """
    def __init__(self, parameters):
        super().__init__(parameters, ['P', 'C', 'F', 'A', 'E', 'N', 'H', 'T', 'Q'])

    def rates(self, y, PFD):
        """
        Method calculating reaction rates.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, E, N, H, T, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            ATPsynthase activity (E), concentration of NADPH (N), lumenal protons (H),
            non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        dict
            A dictionary with the rate names as keys and the rates as values.
        """
        P, C, F, A, E, N, H, T, Q = y

        cs2 = self.crossSectionSimple(T)
        LII = cs2 * PFD
        LI = (1-cs2) * PFD  # assumption: total cross section is always one
        Eact = {1: 1, 0: 0, -1: PFD >= 1, -2: E}[self.par.swATPsynthase]
        vdeep = self.vdeep(Q, H)
        vepox = self.vepox(Q, H)

        v = {
            'ps2': self.vPS2(P, H, vdeep - vepox, LII),
            'ps1': self.vPS1(C, F, LI),
            'ptox': self.vPTOX(P),
            'cytb6f': self.vB6f(P, C, H),
            'ndh': self.vNDH(P),
            'fnr': self.vFNR(F, N),
            'cyc': self.vCyc(P, F),
            'ATPsynthase': self.vATPsynthase(A, H, Eact),
            'ATPconsumption': self.vATPconsumption(A),
            'NADPHconsumption': self.vNADPHconsumption(N),
            'leak': self.vLeak(H),
            'st12': self.vSt12(P, T),
            'st21': self.vSt21(T),
            'deep': vdeep,
            'epox': vepox,
            'cs2': cs2,
            'ATPactivity': self.vATPactivity(PFD, E)
            }

        return v

    def model(self, y, PFD):
        """
        Defining the system of nine equations governing the evolution of the photosynthetic electron transport chain.

        ATPsynthase activation enzyme is light activated.

        Parameters
        ----------
        y: array of float
            Starting conditions. Order:
            P, C, F, A, Eact, N, H, anT, Q = y
            oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
            ATPsynthase activity (E), concentration of NADPH (N), lumenal protons (H),
            non-phosphorylated mobile antennae (T), quencher (Q)

        PFD: float
            Amount of light as photon flux density.

        Returns
        -------
        np.array
            An array containing the value of y for each desired time in t, with the initial value `y0` in the first row.
        """
        # raise error if numerical problems occur regarding excess protonation
        if y[self.H] <= 0:
            raise ModelError("H went below zero")

        # Reaction rates, v, is a dictionary
        v = self.rates(y, PFD)

        # Output from ODE function must be a COLUMN vector, with n rows
        dydt = np.zeros([9, 1])

        # dP/dt = -vPSII + vCytb6f - vcyc + vPTOX - vNDH
        dydt[0] = - v['ps2'] + v['cytb6f'] - v['cyc'] + v['ptox'] - v['ndh']

        # dC/dt = -2vCytb6f + vPSI
        dydt[1] = -2 * v['cytb6f'] + v['ps1']

        # dF/dt = -vPSI + 2vFNR + 2vcyc
        dydt[2] = - v['ps1'] + 2 * v['fnr'] + 2 * v['cyc']

        # dATP/dt = vATPsynthase - vATPconsumption
        dydt[3] = v['ATPsynthase'] - v['ATPconsumption']

        # dATPactivity/dt = vATPactivitiy
        dydt[4] = v['ATPactivity']

        # dN/dt = vFNR - vNADPHconsumption
        dydt[5] = v['fnr'] - v['NADPHconsumption']

        # dH/dt = water_split + proton_pump - synthase_ATP - leak
        dydt[6] = (2 * v['ps2'] + 4 * v['cytb6f'] - self.par.HPR * v['ATPsynthase'] - v['leak']) / self.par.bH

        # dT/dt =  non-phosphorylated antennae // antenna associated with PSII
        dydt[7] = - v['st12'] + v['st21']

        # original MATLAB model with simple quencher
        dydt[8] = v['deep'] - v['epox']

        return dydt

    def vepox(self, Q, H):
        delta_pH = abs(self.par.pHstroma - pH(H))
        kH_deact = self.par.kepox**self.par.nQ / (delta_pH**self.par.nQ + self.par.kepox**self.par.nQ)
        return self.par.kepox * Q * kH_deact
