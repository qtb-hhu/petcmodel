#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from externals.colors import rainbow
import lightprotocol
import lightfunction
import matplotlib.pyplot as plt
import model
import numpy as np
import parameters
import simulate
from time import time

start = time()
__author__ = "Philipp Norf"
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"

# Setting parameters and starting values
p = parameters.Parameters()
# petc model:              P        C      F      A       N        H    anT   Q
y0 = {'PETC': np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0.0000, 0.0001, 1, 0.0000]),
      'Light': np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0, 0.0000, 0.0001, 1, 0.0000])}
# light model:              P        C      F      A   Eact    N       H   anT    Q

"""
# y0 as in the public model
y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0.0000, 0.0001, 1, 0.0000])
# Simulation run and stored
y0 = np.array([1.70731726e+01, 3.71670630e-04, 5.00000000e+00, 3.33041952e-08, 0.00000000e+00, 2.50391755e-05,
               9.24622663e-01,   6.22837684e-11])
"""

# Simulate with 60 seconds interval
# setting default parameters (in LightDict) for light protocol phases
l = lightprotocol.LightProtocol(pfd_start=0, pfd_later=0, pam='0_90', pfd_pulse=5000, dt_pulse=0.6, light_step=10)

# Creating light protocol
l.create(length=190, pam='10_90')
l.create(length=540, pfd_start=60, pfd_later=60)
l.create(length=900)
lfn = lightfunction.LightFunction(l, '_constant')


# running simulation
f, axarr = plt.subplots(3, 1, sharex=True, sharey=True, squeeze=False)


y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0, 0.0000, 0.0001, .5, 0.0000])
m = model.LightModel(p)
lfn.set_counter(0)
s = simulate.Simulate(m, lfn, y0)
s.piecewise_constant()
s.finish(f, axarr)

# plotting results
c = rainbow(4)  # + 1 to avoid yellow as line colour
T, F, Fmax, Tm, Fm, Ts, Fs, Bst, CS2 = s.get_fluo()
axarr[0, 0].plot(T, F/Fmax, '-', color=next(c))  # plot fluorescence trace
axarr[0, 0].plot(Tm, Fm/Fmax, '^:', color=next(c))  # plot maximum fluorescence (more or less NPQ)
axarr[0, 0].plot(Ts, Fs/Fmax, 'v--', color=next(c))

y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0, 0.0000, 0.0001, .75, 0.0000])
m = model.LightModel(p)
lfn.set_counter(0)
s = simulate.Simulate(m, lfn, y0)
s.piecewise_constant()
s.finish(f, axarr)
c = rainbow(4)  # + 1 to avoid yellow as line colour
T, F, Fmax, Tm, Fm, Ts, Fs, Bst, CS2 = s.get_fluo()
axarr[1, 0].plot(T, F/Fmax, '-', color=next(c))  # plot fluorescence trace
axarr[1, 0].plot(Tm, Fm/Fmax, '^:', color=next(c))  # plot maximum fluorescence (more or less NPQ)
axarr[1, 0].plot(Ts, Fs/Fmax, 'v--', color=next(c))

y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0, 0.0000, 0.0001, 1, 0.0000])
m = model.LightModel(p)
lfn.set_counter(0)
s = simulate.Simulate(m, lfn, y0)
s.piecewise_constant()
s.finish(f, axarr)
c = rainbow(4)  # + 1 to avoid yellow as line colour
T, F, Fmax, Tm, Fm, Ts, Fs, Bst, CS2 = s.get_fluo()
axarr[2, 0].plot(T, F/Fmax, '-', color=next(c))  # plot fluorescence trace
axarr[2, 0].plot(Tm, Fm/Fmax, '^:', color=next(c))  # plot maximum fluorescence (more or less NPQ)
axarr[2, 0].plot(Ts, Fs/Fmax, 'v--', color=next(c))
f.text(0.5, 0.04, 'Time [s]', ha='center')
f.text(0.04, 0.5, 'Normalised fluorescence', va='center', rotation='vertical')

print('elapsed time: ', (time()-start))
from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('insilico_.pdf')
pp.savefig(f)
pp.close()
plt.show()
