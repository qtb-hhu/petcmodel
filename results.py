#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

# Fixme: Results.plot_rates() might be broken, but it's pretty much redundant anyway

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from externals.colors import rainbow
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter, ScalarFormatter
from miscellaneous import pH
import model
import numpy as np

__author__ = ["Anna Matuszyńska", "Philipp Norf"]
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszynska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class Results:

    def __init__(self, s, figure=None, axarr=None):
        """
        Initializes a Results instance from a passed Simulate instance. Under normal operation this isn't required as
        the Results instance is created by changing the class of a Simulate instance by calling the finish()-method of
        of Simulate on an Simulate instance.

        Parameters
        ----------
        s: Simulate
            Instance of a Simulate class.
            Note that it is possible to initialize a Results instance from a Simulate instance that is empty. Make sure
            that this is not the case.

        figure: figure object
            A figure object created by plt.subplots. Optional.

        axarr: axis object
            An axis object created by plt.subplots. Optional.
        """
        # Attributes that are adopted from Simulate
        self.model = s.model
        self.results = s.results
        self.lfn = s.lfn

        # Attributes that are inherent to Results
        self.figure, self._axarr, self.axis = None, None, None  # Defined in __init__ for clarity. Filled by methods
        self.set_subplots(figure, axarr)

    def __call__(self):
        """
        Configures the call to the instance to process and return all processed results.
        Look up attribute results to get to the raw results.

        Returns
        -------
        dict
            Dictionary of all results.
        """
        results = {'t': self.get_t(), 'light': self.get_light()}

        m = self.model.__class__
        # TODO: Maybe move keys to model.py so that we only have to lock the available model classes and extract keys
        keys = {model.PETCmodel: {'rates': ('ps2', 'ps1', 'ptox', 'cytb6f', 'ndh', 'fnr', 'cyc', 'ATPsynthase',
                                            'ATPconsumption', 'NADPHconsumption', 'leak', 'st12', 'st21', 'quencher',
                                            'cs2'),
                                  'var': ('P', 'C', 'F', 'A', 'N', 'H', 'anT', 'Q'),
                                  'fluo': ('F', 'Fmax', 'Tm', 'Fm', 'Ts', 'Fs', 'Bst', 'CS2')}}

        for i in range(len(keys[m]['var'])):
            results[m['var'][i]] = self.get_var(i)

        for i in range(len(keys[m]['rates'])):
            results[m['rates'][i]] = self.get_rate(i)

        fluo = self.get_fluo()
        for i in range(len(keys[m]['fluo'])):
            results[keys[m]['fluo'][i]] = fluo[i + 1]

        return results

    def set_subplots(self, figure, axarr, current_subplot=None):
        """
        Sets the subplot by storing the passed figure object, axis object and selecting a current subplot.

        Parameters
        ----------
        figure: figure object
            A figure object created by plt.subplots.

        axarr: axis object
            An axis object created by plt.subplots.

        current_subplot: numpy.array
            Indices to select the current axis. Defaults to first subplot if set to None (default value).
        """
        if isinstance(figure, matplotlib.figure.Figure):  # rest of check always fails due to matplotlib voodoo...
            # and isinstance(axarr, matplotlib.axes._subplots.AxesSubplot):
            self.figure, self._axarr = figure, axarr
        else:
            self.figure, self._axarr = plt.subplots()

        self.select_subplot(current_subplot)

    def select_subplot(self, axis):
        """
        Selects the current subplot by the given indices of the axis array or sets the current subplot to the first
        subplot of the indices are out of range or mismatching the shape of the axis array.

        Parameters
        ----------
        axis: numpy.array
            Indices to select the current axis.
        """
        # a few explicit type conversion because numpy bricks the duck typing
        try:
            axis = tuple(axis)
        except TypeError:
            axis = tuple(np.zeros(len(np.shape(self._axarr)))) if axis is None else axis

        try:
            self.axis = self._axarr[axis]

        except TypeError:  # TypeError only occurs if self._axarr is a single plot.
            self.axis = self._axarr

    # Functions to extract values
    def get_t(self, r=None):
        """
        Extracts all time points within the selected range of the simulation.

        Parameters
        ----------
        r: range or list
            Range of steps of simulation for which results we are interested in.

        Returns
        -------
        numpy.array
            All time points within the selected range of the simulation.
        """

        r = range(len(self.results)) if r is None else r

        return np.hstack([self.results[i]['t'] for i in r])

    def get_var(self, target, r=None):
        """
        Extracts all concentrations of a target variable from simulation.results attribute and returns them as vector.

        Parameters
        ----------
        target: int or IntEnum
            Index of the state variable. Remember that the order of state variables is stored as integer enumeration in
            Model._names and accessible with Model.X where X is the name of the state variable.
            For the release version of the model (PETCModel) the order is: P, C, F, A, N, H, T, Q.
                oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
                concentration of NADPH (N), lumenal protons (H),  non-phosphorylated mobile antenna (T), quencher (Q)

            For the modified version of the model (LightModel) the order is: P, C, F, A, E, N, H, T, Q.
                oxidised plastoquinone (P), plastocyan (C), ferrodoxin (F), concentration of ATP (A),
                ATPsynthase activity (E), concentration of NADPH (N), lumenal protons (H),
                non-phosphorylated mobile antennae (T), quencher (Q)
        r: range or list
            Range of steps of simulation for which results we are interested in.

        Returns
        -------
        numpy.array
            Concentrations of target variable as one vector.
        """
        r = range(len(self.results)) if r is None else r

        return np.hstack([self.results[i]['y'][:, target] for i in r])

    def get_light(self, r=None):
        """
        Extracts the light intensities at every time point within the selected range of the simulation.

        Parameters
        ----------
        r: range or list
            Range of steps of simulation for which results we are interested in.

        Returns
        -------
        list of Number
            List of all light intensities at every time point within the selected range of the simulation.
        """
        if r is None:
            r = range(len(self.results))

        light = []
        for i in range(len(r)):
            self.lfn.set_counter(self.results[i]['lfn_counter'])
            np.hstack([light, [self.lfn(t) for t in self.get_t()]])

        return light

    def get_rate(self, rate, r=None):
        """
        :param rate: name of the rate
        :param r: range of steps of simulation for which results we are interested in
        :return: rate with name 'rate' of all results as one vector
        """
        r = range(len(self.results)) if r is None else r

        v = np.array([])

        for i in r:
            t = self.results[i]['t']
            y = self.results[i]['y']
            self.lfn.set_counter(self.results[i]['lfn_counter'])

            v = np.hstack([v, np.array([self.model.rates(y[j], self.lfn(t[j]))[rate] for j in range(len(t))])])

        return v

    def get_fluo(self, r=None):
        """ return values required for plotting fluorescence traces
            return: time vector, overall fluorescence from PSII, PSII yield, fluorescence emitted from open reaction centres,
            fluorescence emitted from closed reaction centres, state of the PSII, relative cross section of PSII
        """

        if r is None:
            r = range(len(self.results))

        Fm = []  # maximal fluorescence
        Fs = []  # base fluorescence
        F = []   # total fluorescence trace (flashes)
        T = []
        Tm = []  # time for maximal fluorescence (flashes)
        Ts = []
        Fmax = 0
        Bst = np.array([]).reshape(0, 4)
        CS2 = []

        for i in r:
            t = self.results[i]['t']
            y = self.results[i]['y']
            self.lfn.set_counter(self.results[i]['lfn_counter'])
            P = y[:, self.model.P]
            A = y[:, self.model.T]  # A for Antennae (avoids collision)
            X = y[:, self.model.Q]  # X for xanthophyll (avoids collision)
            H = y[:, self.model.H]

            Q = self.model.vdeep(X, H) - self.model.vepox(X, H)
            cs2 = self.model.crossSectionSimple(A)
            CS2 = np.hstack([CS2, cs2])

            B = np.array([self.model.ps2states(P[j], Q[j], cs2[j] * self.lfn(t[j])) for j in range(len(t))])

            T = np.hstack([T, t])
            Bst = np.vstack([Bst, B])

            # The fluorescence signal comes from two states: from state B1 is proportional to kF /(kH + kF + kPchem )
            # and that resulting from B3 is proportional to kF /(kH + kF ).
            # Further, the fluorescence signals are proportional to the occupation of the two respective ground states
            # and both are proportional to the cross-section of PSII.

            Fst0 = cs2 * self.model.par.kF / \
                (self.model.par.kF + self.model.par.kH0 + self.model.par.kH * Q + self.model.par.k2) * B[:, 0]
            Fst2 = cs2 * self.model.par.kF / \
                (self.model.par.kF + self.model.par.kH0 + self.model.par.kH * Q) * B[:, 2]

            F = np.hstack([F, Fst0 + Fst2])

            # Base fluorescence F0 is observed when all reaction centres are open
            # Maximal fluorescence FM is observed under saturating flashes

            if np.isclose(self.lfn(t[0]), 5000):  # FIXME: hardcoded pulse photon flux density.
                Tm = np.hstack([Tm, t[np.argmax(Fst0 + Fst2)]])
                Fm = np.hstack([Fm, max(Fst0 + Fst2)])
            else:
                Ts = np.hstack([Ts, t[np.argmin(Fst0 + Fst2)]])
                Fs = np.hstack([Fs, min(Fst0 + Fst2)])

            # Fmax is the fluorescence at the first saturating pulse after dark adaption. Not necessarily the first pfd.
            # actually this is somewhat pointless. max(F) is the same and we've to store F anyway.
            if not Fmax and self.lfn(t[0]) == 5000:  # FIXME: hardcoded pulse photon flux density.
                Fmax = max(Fst0 + Fst2)

        return T, F, Fmax, Tm, Fm, Ts, Fs, Bst, CS2

    # Functions to plot results
    def plot_ph(self, r=None, **kwargs):
        """ plot lumenal pH """
        r = range(len(self.results)) if r is None else r

        self.axis.plot(self.get_t(r), pH(self.get_var(self.model.H, r)), **kwargs)

        self.axis.set_xlabel('time [s]')
        self.axis.set_title('Lumenal pH')
        self.axis.legend(loc='best')

    def plot_rel(self, r=None, colors=None, **kwargs):
        """ plot results of integration
            six plots for reduced PQ, PC, Fd, and concentration of ATP and  NADPH + cross section of PSII
        """
        r = range(len(self.results)) if r is None else r
        colors = rainbow(5) if colors is None else colors

        t = self.get_t(r)
        ys = {'PQred': 1 - self.get_var(self.model.P) / self.model.par.PQtot,
              'PCred': 1 - self.get_var(self.model.C) / self.model.par.PCtot,
              'Fdred': 1 - self.get_var(self.model.F) / self.model.par.Fdtot,
              'ATP':       self.get_var(self.model.A) / self.model.par.APtot,
              'NADPH':     self.get_var(self.model.N) / self.model.par.NADPtot}

        for label, y in ys:
            self.axis.plot(t, y, color=next(colors), label=label, **kwargs)

        self.axis.set_xlabel('time [s]')
        self.axis.set_title('Temporal evolution of state variables')
        self.axis.legend(loc='best')

    def plot_rates(self, rates, r=None, colours=None, **kwargs):
        """ plot selected reaction rate
        :param rate: name of one of the reactions
        :return: plot reaction rate of a selected rate
        """
        r = range(len(self.results)) if r is None else r
        c = rainbow(len(rates) + 1) if colours is None else colours  # + 1 to avoid yellow as line colour
        t = self.get_t(r)
        x = {rate: self.get_rate(rate, r) for rate in rates}

        for k, v in x:
            self.axis.plot(t, x, label=k, color=next(c), **kwargs)

        self.axis.set_xlabel('time [s]')
        self.axis.set_title('Reaction rates')
        self.axis.legend(loc='best')

    def plot_fluo(self, r=None, **kwargs):
        """ plot fluorescence trace, uses function fluo to calculate FM and F0 """
        r = range(len(self.results)) if r is None else r
        T, F, Fmax, _, _, _, _, _, _ = self.get_fluo(r)

        self.axis.plot(T, F/Fmax, **kwargs)

        self.axis.set_xlabel('time [s]')
        self.axis.set_title('Normalised fluorescence trace')
        self.axis.legend(loc='best')

    def plot_raw_vars(self, vars, r=None, colors=None, **kwargs):
        """ plot state of the raw variables uses function getT and getVar to extract variables """
        r = range(len(self.results)) if r is None else r
        colors = rainbow(len(vars)) if colors is None else colors
        t = self.get_t(r)

        for var in vars:
            self.axis.plot(t, self.get_var(var, r), color=next(colors), **kwargs)

        self.axis.set_xlabel('time [s]')
        self.axis.set_title('States of the raw variables')
        self.axis.legend(loc='best')

    def plot_light(self, r=None, **kwargs):
        """ plot function of light used in the simulation
        """
        r = range(len(self.results)) if r is None else r

        self.axis.plot(self.get_t(r), self.get_light(r), **kwargs)
        self.axis.set_title('Light function')

    def plot_ps2(self, r=None, colors=None, **kwargs):
        r = range(len(self.results)) if r is None else r
        colors = rainbow(len(vars)) if colors is None else colors
        T, _, _,  _,  _,  _,  _,  Bst, _ = self.get_fluo(r)

        for i in range(4):
            self.axis.plot(T, Bst[:, i], label='B'+str(i), color=next(colors), **kwargs)

        self.axis.set_xlabel('time')
        self.axis.set_title('Occupation of states in the photosystem II')
        self.axis.legend(loc='best')

# Never changed, never tested. might be obsolete.
# def plot_steadystate_surface(st_range, pfd_range, Ys, target='PQ'):
#     fig = plt.figure()
#     ax = fig.gca(projection='3d')
#
#     x, y = np.meshgrid(pfd_range, st_range)
#     yss = np.zeros([len(st_range), len(Ys)])
#
#     if target == 'PQ':
#         plt.title('steady state of reduced plastoquinon pool')
#         ax.set_zlim(0, 1)
#         for i in range(len(st_range)):
#             for j in range(len(pfd_range)):
#                 yss[i, j] = 1 - Ys[i][j][0] / 17.5
#
#     else:  # target == 'pH'
#         plt.title('Steady state of the pH')
#         ax.set_zlim(0, 7.2)
#         for i in range(len(st_range)):
#             for j in range(len(pfd_range)):
#                 yss[i, j] = pH(Ys[i][j][5])
#
#     surf = ax.plot_surface(x, y, yss, rstride=1, cstride=1, cmap=cm.get_cmap('jet'), linewidth=1, antialiased=True)
#     y_formatter = ScalarFormatter(useOffset=False)
#
#     ax.yaxis.set_major_formatter(y_formatter)
#     ax.zaxis.set_major_locator(LinearLocator(10))
#     ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
#     fig.colorbar(surf)
