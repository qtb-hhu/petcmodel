#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from enum import IntEnum
from numpy.core.umath import log

__author__ = ["Anna Matuszyńska", "Philipp Norf"]
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class Switch(IntEnum):
    """
    Enumeration to be used as states of a parameter switch.
    Some store a constant that is used directly.
    """
    # Provoke errors if uses as direct return values
    kinetics = -1
    direct = -1

    # Used as direct return values
    constant = 1
    inactive = 0


class ModelError(Exception):
    """
    Error specific to the PETCmodel. Capable of saving additional information using the 'who' attribute, intended to
    store the name of the function that raised the error, and the 'meta' attribute, intended to store some additional
    in formation about. Both is done via **kwargs which is build into all errors but, strangely enough, always ignored.
    """
    def __init__(self, *args, comment=None, who=None, **kwargs):
        super().__init__(*args, **kwargs)
        if comment is not None:
            self.comment = str(comment)

        if who is not None:
            self.who = who


# Unit conversions
def pH(x):
    return -log(x*2.5e-4)/log(10)


def pHinv(ph):
    return 4e3*10**(-ph)
