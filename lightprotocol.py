#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

# TODO: for all protocols: plt.plot(tfls, pfds) and pay attention to the light flashes Also needed for documentation

# TODO: maybe move all classes in their own files and make them a module that's shared between model and pbr project

# KNOWN BUGS
# FIXMED?: light_step, the linear protocol or remove_redundancy might be broken.

# FIXME: This needs a Flag that allows to toogle between light protocols for simulation and switching real lights.
# real:         leading zero and times when light is switched to (!) a specific state
# simulation:    zero added by integrator and times when light switched from (!) a specific state
# The simulation takes tn and t[n-1] to integrate, real lights need tn and tm with m > n

# FIXED?: LightProtocol.calculate: remove leading zero! TODO: where was this done?

# FIXED? Phase.__getattr__ merge pam+dt_pulse into pfd_actinic, not pam alone! (also resolves bad convergence thing)


# LESS IMPORTANT
# FIXME: SINE PROTOCOL: always shift offset in a way so that the curve starts at the right value

# FIXME: last pfd before flash is set to >>(pfds_actinic[0] + pfds_actinic[-1])/2<< if pam[0] = 0
# qickfix: filter it out and set it to pfd[0]; more sophisticated: fix code that generates last pfd before flash

# FIXME: LightDict: sun seems to be always invalid. Hence, lambda_sun is probably flawed.

# FIXME: LightDict: Phases shouldn't store parameters that are default_parameters but not part of the phases protocol.

# IDEAS FOR ADDITIONAL PROTOCOLS
# TODO: implement saturation function (easy, just take the half-sigmoid function for that)

# TODO: allow to pass user defined functions to anything that creates a pattern like the PAM parameter

# TODO: implement square, triangle and sawtooth wave as special cases of sine
# TODO: implement sine cardinalis (sinc)
# If i'm not complete off these two involve Fourier transformations:
#       https://en.wikipedia.org/wiki/Fourier_transform
#        That's gonna take a while...

# TODO: Add another function to plot the light protocol: The actual state of the light
# (Because light protocols only give the times a light is switched; The light stays constant until the next switch time)

# RANDOM THOUGHTS / NOTES / Nice_to_have_but_not_vital stuff
# TODO: code could be more pythonic in terms of input validation

# TODO: Oliver wanted something that recreates a given spectra with the available light source.
# If the light source is made up of different LEDs (for this any kind of RGB or multicolor LEDs are 3 (or more) separate
# LEDs) this is a matter of dimming the LEDs correctly which in turn is a matter of scaling the known spectra of each
# single LED. This can be achieved by stacking the spectra onto each other and then finding the best compromise between
# the scaling factors for each spectra. Basically this is pretty close to fitting a polynome of degree N or less where N
#  is the number of LEDs.

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from collections import OrderedDict, Iterable
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import matplotlib.colors as cls
from miscellaneous import ModelError
from externals.colors import rainbow
from externals.words import gibberish
from externals.norm_tools import normalise
from numbers import Number
import numpy as np
import re

__author__ = "Philipp Norf"
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class LightProtocol:
    """
    This class builds a light protocol.
    This approach follows the idea, that a full light protocol can be described as sequence of phases of distinct
    lighting conditions. Each phase is calculated and stored separately. Each phase protocol is defined by its
    parameters so there is no need to state which protocol should be used. Also, it is not required to pass all
    parameters, as missing parameters always will be filled with default parameters.

        See the documentation of LightDict class for more information on parameters and phase protocols.
        See the documentation of Phase class for more information on single phases.
        See the documentation of LightProtocol.__getattr__ and LightProtocol.__getitem__ to learn how to access the
        stored data.
    """
    def __init__(self, **default_parameters):
        """
        Initialises the instance and takes a dictionaries of default parameters in the form of keyword arguments. These
        are evaluated by the method set_default_parameters of the LightDict class which always performs a validation of
        passed parameters. Parameters that do not pass this evaluation are replaced by LightDicts fail save default
        parameters. Hence, the set of default parameters is always complete.

        Parameters
        ----------
        default_parameters
            Parameters as keyword arguments. Recommended:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs
        """
        LightDict.set_default_parameters(**default_parameters)
        self.phases = []

    def __getattr__(self, attr):
        """
        The magic method __getattr__ is automatically called as soon as an attribute is not found in an object. Here it
        is used to dynamically generate attributes that depend on other, more static attributes. To access slices of the
        stored light protocol see magic method __getitem__. To avoid redundant code __getattr__ redirects to __getitem__
        for light protocol slices while __getitem__ redirects to __getattr__ for everything else, including dealing with
        missing keys (or attributes, respectively).

        Parameters
        ----------
        item: str
            item is extracted from instance.item. It is the name of the looked up attribute.
        Returns
        -------
        Attributes
            Returns the dynamically generated attribute or raises an AttributeError
        """
        # Dynamically converting lists/dictionaries to a np.arrays  # Fixme: is this used extensively? if not remove!
        # the np.array is more convenient from slicing, the list for editing (basically artificial duck-typing here)
        if attr.startswith('arr_'):
            out = getattr(self, attr[4:])
            return np.array(list(out.items()) if isinstance(out, dict) else out)

        # Getting phase indices:
        if attr == 'pids':
            return [phase['id'] for phase in self.phases]

        # Getting a single phase by its pid
        if attr in self.pids:
            return self.phases[self.pids.index(attr)]

        # Getting tfls/pfds (full light protocol)
        if attr in ['tfls', 'pfds', 't_actinic', 'pfds_actinic']:
            return self[self.pids][attr] if self.phases else np.array([])

        # Getting full protocol as dictionary (like getitem)
        if attr is 'lightprotocol':
            return self()

        raise ModelError("'LightProtocol' object has no attribute or key named " + attr, who=self.__getattr__)

    def __getitem__(self, keys):
        """
        The magic method __getitem__ is automatically called as soon as an subscript of an object is called.
        Here it is used to pass an ordered list of phase IDs that represent a slice of the stored light protocol which
        then is dynamically generated and returned. To avoid redundant code __getattr__ redirects calls for full light
        protocol to __getitem__. __getitem__ in turn redirects to __getattr__ for everything that isn't a light protocol
        slice and dealing with missing keys (or attributes, respectively).

        Parameters
        ----------
        keys: str or tuple
            keys is extracted from instance[keys]. It is the key of the looked up item.
        Returns
        -------
        Attributes
            Returns the dynamically generated attribute or raises an AttributeError
        """
        # redirecting to __call__ (returns full light protocol) for keys that evaluate as False.
        if not bool(keys):
            return self()

        # redirecting to self.__getattr__ for everything that's not a slice of a light protocol (also single phases)
        if keys in self.pids or not all([pid in self.pids for pid in keys]):
            return getattr(self, keys)

        # Getting a slice of light protocol
        else:
            selection = self.arr_phases[[np.where(self.arr_pids == pid)[0][0] for pid in keys]]

            # extracting and processing length correction factors
            lengths = [phase['parameters']['length'] for phase in selection]
            lengths = iter(np.array([[sum(lengths[0:i])] for i in range(len(lengths))]))

            # extracting, applying length correction factors and returning output
            return {'tfls': np.hstack([phase['tfls'] + next(lengths) for phase in selection]),
                    'pfds': np.hstack([phase['pfds'] for phase in selection]),
                    't_actinic': np.hstack([phase['t_actinic'] for phase in selection]),
                    'pfds_actinic': np.hstack([phase['pfds_actinic'] for phase in selection])}

    def __call__(self):
        """
        The magic method __call___ is automatically called if the instance itself is called.
        Here it is used to return the full protocol in the same format as a protocol slice would be returned. To avoid
        redundant code __call__ redirects most of the processing to __getitem__.

        Returns
        -------
        dict
            Dictionary containing photon flux densities (pfds), time points at which the light changes (tfls), time
            time points at which the actinic light changes (t_actinic) and the actinic light intensities (pfds_actinic).
        """
        # Getting full protocol as dictionary (like getitem)
        return self[self.pids] if self.phases else {k: np.array([]) for k in ('tfls', 'pfds', 't_actinic',
                                                                              'pfds_actinic')}

    def get_pid_from_t(self, t, *pids, t0=0):
        """
        Looks up the phase for a certain time point in the light protocol specified by the passed phase IDs. The object
        assumes a call to the full light protocol if no phase IDs are passed.

        Parameters
        ----------
        t: Number
            Time point to be looked up.

        pids: str
            Phase IDs (ordered) to specify a light protocol deviating from the full light protocol.

        t0: Number
            Time at which the light protocol starts.

        Returns
        -------
        str
            Phase ID that contains time t.
        """
        try:
            prev_t = t0
            pids = (False,) if pids is None else pids  # Fixme: this line is broken
            for pid in pids:
                phase = self[pid]
                if any(phase['tfls'] + prev_t >= t):
                    return pid
                else:
                    prev_t = phase['tfls'][-1]
            else:
                raise IndexError("'t' lies out of range of tfls.")

        except (ModelError, KeyError):
            ValueError("The passed arguments '{}' are not valid phase IDs."
                       .format("', '".join(str(pid) for pid in set(pids).difference(self.l.pids))))

    @staticmethod
    def calculate(dummy=False, **parameters):
        """
        This method calculates a single phase of a light protocol and returns a temporary Phase object or the calculated
        values as dictionary, e.g. for control or immediately plotting a light protocol. To create a permanently stored
        new phase, use the method called create. It is strongly recommended to not store calculate phases manually!

        Note: diurnal is approximated by modifying a Gauss curve. This worked quite well so far but does not exactly hit
        the sunrise/sunset times. However, they can be corrected quite easily by playing around with the zenith value of
        misc.sunrise_equation.

        Parameters
        ----------
        dummy: bool
            If True, calculate returns a Phase object dummy instead of t_actinic and pfds_actinic arrays.

        parameters
            Parameters as keyword arguments. This method uses LightDict class to store and validate the parameters:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs

        Returns
        -------
        temporary Phase object
            phase as temporary instance of the Phase object. Not stored in self.phases and not valid in terms of phases.
        OR
        dict of np.arrays
            calculated times of changes of the actinic light (t_actinic) and photon flux densities (pfds_actinic).

        """
        # preliminary actions: checking, determining, completing
        parameters = LightDict(**parameters)
        protocol = LightDict.protocol(**parameters)

        # Preallocating stuff
        t_actinic = np.array([])
        pfds_actinic = np.array([])

        # processing protocol
        if protocol is 'sine':
            def sine(t, pfd_low, pfd_high, period, offset):
                # takes np.array as t, returns f_(t), takes all other variables from outer scope.
                a = (pfd_high - pfd_low) / 2.0
                return a * np.sin(period * t + offset) + a + pfd_low

            # calculating t_actinic and pfds_actinic
            pfd_low = min([parameters['pfd_start'], parameters['pfd_later']])
            pfd_high = max([parameters['pfd_start'], parameters['pfd_later']])
            # t_actinic = np.arange(0, parameters['length'], parameters['dt_light'])  # alt.
            t_actinic = np.arange(parameters['dt_light'], parameters['length'], parameters['dt_light'])
            pfds_actinic = sine(t_actinic, pfd_low, pfd_high, parameters['period'], parameters['offset'])

        elif protocol is 'diurnal':
            def diurnal(day_length, sunrise, sunset, high_noon_light):
                # time course of a day
                t = np.arange(0, day_length, 0.2)  # Fixme: 0.2 ??? Why not dt_light?

                # simulating daylight intensity as modified Gauss curve
                # this is just an approximation to sunrise/sunset time but way easier!
                # this easily can be adjusted by using a different zenith value for misc.sunrise_equation()
                sigma = (sunset - sunrise) / (np.pi * 2)  # the approx. happens here!
                mean = (sunset - sunrise) / 2. + sunrise  # high noon, contrary to popular believe it's not always 12am!
                y = mlab.normpdf(t, mean, sigma)
                y *= high_noon_light / max(y)  # scaling to max light intensity

                # correction for proper night times (constant pfd: 0)
                # t = np.hstack([0, t[np.where(y > 0.5)], max(t)])  # alt.
                # y = np.hstack([0, y[np.where(y > 0.5)], 0])  # alt.
                t = np.hstack([t[np.where(y > 0.5)], max(t)])
                y = np.hstack([y[np.where(y > 0.5)], 0])
                return t, y

            if len(parameters['sun']) < 2:  # always take the same sunrise/sunset times
                t, y = diurnal(parameters['day_length'], *parameters['sun'][0], parameters['pfd_later'])
                for d in range(int(np.ceil(parameters['length'] / parameters['day_length']))):
                    t_actinic = np.hstack([t_actinic, t + parameters['day_length'] * d])
                    pfds_actinic = np.hstack([pfds_actinic, y])
                # truncating round-off error
                t_actinic = t_actinic[np.where(t_actinic <= parameters['length'])]
                pfds_actinic = pfds_actinic[np.where(t_actinic <= parameters['length'])]

            else:  # dynamically changing them
                for d in range(len(parameters['sun'])):
                    t, y = diurnal(parameters['day_length'], *parameters['sun'][d], parameters['pfd_later'])
                    t_actinic = np.hstack([t_actinic, t + parameters['day_length'] * d])
                    pfds_actinic = np.hstack([pfds_actinic, y])

        else:  # viz.: elif protocol in ['constant', 'linear', 'sigmoid']
            def sigmoid(t_max, pfd_start, pfd_later, delay, modifier, dt_light):
                # calculates half a sigmoid below and above the inflexion point and np.hstacks them together.
                # if modifier is 0, the inner, normalized term collapses to the linear equation f(t) = t.
                def half_sigmoid(t, k):  # describes curve from (0,0) to point of inflexion (1,1)
                    return (k*t - t) / (2.*k*t - k - 1)

                t = np.arange(0, 1, dt_light * 2 / (t_max - delay))

                pfd_low = min([pfd_start, pfd_later])
                pfd_high = max([pfd_start, pfd_later])
                lr = (pfd_high - pfd_low) / 2.  # light range

                # calculating curve below point of inflexion
                y_below = lr * half_sigmoid(t, modifier) + pfd_low

                # calculation curve above point of inflexion
                y_above = lr * half_sigmoid(t, -modifier) + pfd_high - lr

                # creating output
                t = np.hstack([t, t + 1])
                t = t / 2 * (t_max - delay) + delay
                f_t = np.hstack([y_below, y_above])
                if pfd_start > pfd_later:
                    f_t = f_t[::-1]
                t = t[1:]  # removes leading zero
                f_t = f_t[1:]

                if delay:
                    t = np.hstack([dt_light, t])  # it's working, but why?! Shouldn't it be t+dt_light instead of t
                    f_t = np.hstack([pfd_start, f_t])

                return t, f_t

            # calculating t_actinic and pfds_actinic
            t_actinic, pfds_actinic = sigmoid(parameters['length'], parameters['pfd_start'], parameters['pfd_later'],
                                              parameters['delay'], parameters['modifier'], parameters['dt_light'])

        # add last point of phase in case it is missing
        if parameters['length'] not in t_actinic:
            t_actinic = np.append(t_actinic, parameters['length'])
            pfds_actinic = np.append(pfds_actinic, pfds_actinic[-1])

        # remove all redundant points
        t_actinic, pfds_actinic = remove_redundancy(t_actinic, pfds_actinic, parameters['light_step'])

        if dummy:
            return Phase(None, t_actinic, pfds_actinic, parameters)
        else:
            return {'t_actinic': t_actinic, 'pfds_actinic': pfds_actinic}

    def create(self, **parameters):
        """
        This method is a wrapper for the calculate method which only calculates but not saves protocols. create makes
        sure that a valid protocol is assigned a random unique ID and properly saved as a phase object. Use this method
        for creating new phases!

        Parameters
        ----------
        parameters
            Parameters as keyword arguments. This method uses the LightDict class to validate the parameters:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs

        Returns
        -------
        str
            Phase ID of the newly created phase.

        """
        # Tries to create a Phase object from parameters, calls self.calculate which in turn calls self.parameters
        try:
            parameters = LightDict(**parameters)
            pid = gibberish(5, self.pids)  # random name as phase ID
            p = self.calculate(**parameters)
            self.phases.append(Phase(pid, p['t_actinic'], p['pfds_actinic'], parameters))

            m = ['Added phase {0} calculated from following parameter:'.format(pid)]
            for k, v in parameters.items():
                m.append('\t{0}:\t\t{1}'.format(k, str(v)))

        except (ModelError, ValueError, KeyError) as m:  # Not sure if KeyError may occur at all but won't risk it.
            pid = None
            m = ['Invalid parameter set. No new phase added.', m]

        # print('\n'.join(m))  # FIXME: UnboundLocalError: local variable 'm' referenced before assignment
        # try-block succeed: m is defined, try-block fails: m is defined. WHY AN ERROR?!
        # maybe the error message container m is meant...
        return pid

    def arrange(self, pids):
        """
        Rearranges the phase objects in phases by given phase IDs.

        Parameters
        ----------
        pids: Iterable of str
            Phase IDs in desired order. For best results use a iterable that stores order!
        """
        if set(pids) == set(self.pids):  # sets because order is not important
            self.phases = list(self.arr_phases[[np.where(self.arr_pids == pid)[0][0] for pid in pids]])
        else:
            raise ValueError('Expected a list of valid phase IDs. Got {0} instead'.format(str(pids)))

    def delete(self, pid):
        """
        Deletes the phase with the given phase ID from phases.

        Parameters
        ----------
        pid: str
            Phase ID.
        """
        if pid in self.pids:
            del self.phases[np.where(self.arr_pids == pid)[0][0]]
        else:
            raise ModelError(str(pid) + ' is not a valid phase ID!', who=self.delete)

    def clone(self, pid, number):
        """
        Clones a phase n times and rearranges self.phases so that the original phase is followed up by its clones.

        Parameters
        ----------
        pid: str
            Phase ID.

        number: int
            Number of clones to create.

        Returns
        -------
        list or str
            list of phase IDs starting with the original phase followed by its clones.
        """
        try:
            phase = getattr(self, pid)
            pos = np.where(self.arr_pids == pid)[0][0]
            pids = [pid]
            for n in range(number):
                clone = phase.copy()
                clone['id'] = gibberish(5, self.pids)
                pids.append(clone['id'])
                self.phases.append(clone)
            self.arrange([*self.pids[:pos], *pids, *self.pids[pos+1:-number]])
            return pids

        except ModelError:
            raise ModelError(str(pid) + ' is not a valid phase ID!', who=self.clone)
        except TypeError:
            raise ValueError("'Number' expected type 'int' and greater 0. Got '{0}' with type '{1}' instead."
                             .format(str(number), type(number).__name__))

    def fuse(self, pids, rescale=True, boundaries=None, purge=True):
        """
        Fuses two  or more light protocols together to create a combined light protocol. Thus allowing for the creation
        of more complex phases. By default the combined phase is rescaled to fit the range of the initial phases if an
        iterable of two numbers is given the rescaling happens within that margin. After fusing the initial phases are
        deleted, if not stated otherwise.

        Combining any protocol with a constant protocol just shifts the protocol on the y-axis.
        All other combinations create more or less obscure curves. Combining anything with a weak sine curve probably is
        a great way to simulate light fluctuation similar to clouds.

        Parameters
        ----------
        pids: Iterable of str
            Phase IDs to be fused.

        rescale: bool
            If True, the combined phase is rescaled to fit the range given in scaling or those of the initial phases.
            Pulses are not rescaled!

        boundaries: None or Iterable (2) of Numbers or None
            If rescale is True, boundaries sets the lower and upper boundary for rescaling the fused phase. If
            boundaries is nonsense or not set at all the boundaries of the original phases are reused.

        purge: bool
            If True, the initial phases will be deleted afterwards.

        Returns
        -------
        str
            Phase ID of the fused phase.
        """
        phase = {'t_actinic': np.array([]), 'pam': np.array([])}
        store = {'length': [], 'dt_pulse': [], 'pfd_pulse': [], 'pfds_actinic': [], 'light_step': []}
        try:  # getting phases and last trait that needs special evaluation (keeping loop short, therefore)
            for pid in pids:
                phase[pid] = getattr(self, pid)
                store['length'].append(phase[pid]['parameters']['length'])
            if not np.allclose(store['length'][0], store['length'][1:]):
                raise ModelError(who='length')
        except ModelError as err:
            msg = {self.__getattr__: 'Expected a list of valid phase IDs. Got {0} instead'.format(str(pids)),
                   'length': "Phases with different lengths can't be merged!"}
            raise ModelError(msg[err.who], who=self.fuse)

        # actually working with phases: 1) extracting parameters, 2) merging, 3) deleting duplicates
        for pid in pids:
            store['dt_pulse'].append(phase[pid]['parameters']['dt_pulse'])
            store['pfd_pulse'].append(phase[pid]['parameters']['pfd_pulse'])
            store['light_step'].append(phase[pid]['parameters']['light_step'])
            pos = np.searchsorted(phase['t_actinic'], phase[pid]['t_actinic'])
            phase['t_actinic'] = np.insert(phase['t_actinic'], pos, phase[pid]['t_actinic'])
            phase['pam'] = np.insert(phase['pam'], pos, phase[pid]['pam'])
        pos = np.where(np.isclose(phase['t_actinic'][1:], phase['t_actinic'][:-1]))
        phase['pam'] = np.delete(phase['pam'], pos)
        phase['t_actinic'] = np.delete(phase['t_actinic'], pos)
        param = {'length': store['length'][0], 'pam': phase['pam'], 'pfd_pulse': max(store['pfd_pulse']),
                 'dt_pulse': max(store['dt_pulse']), 'light_step': min(store['light_step'])}

        # adding intermediate light intensities at positions intermediate tfls were added
        for pid in pids:
            ind = np.where(np.array(
                    [not any(np.isclose(t, phase[pid]['t_actinic'], 0.001)) for t in phase['t_actinic']]))
            pos = np.searchsorted(phase[pid]['t_actinic'], phase['t_actinic'][ind])
            pac = (phase[pid]['pfds_actinic'][pos] + phase[pid]['pfds_actinic'][pos - 1]) / 2
            obj = Phase(None, phase['t_actinic'], np.insert(phase[pid]['pfds_actinic'], pos, pac), param)
            store['pfds_actinic'].append(obj['pfds_actinic'])

        # fusing phases but keeping pulses untouched
        phase['pfds_actinic'] = np.sum(store['pfds_actinic'], 0)
        if rescale:
            try:  # normalise will immediately throw a ValueError if boundaries is nonsense.
                phase['pfds_actinic'] = normalise(phase['pfds_actinic'], boundaries)
            except ValueError:  # ValueError will be interpreted as: "use the upper/lower range of original phases"
                boundaries = [np.min(store['pfds_actinic']), np.max(store['pfds_actinic'])]
                phase['pfds_actinic'] = normalise(phase['pfds_actinic'], boundaries)

        # remove all redundant points
        t_actinic, pfds_actinic = remove_redundancy(phase['t_actinic'], phase['pfds_actinic'], param['light_step'])

        # Adding a fused phase to phases
        new_pid = gibberish(5, self.pids)  # random name as phase ID
        self.phases.append(Phase(new_pid, t_actinic, pfds_actinic,
                           {'length': store['length'][0], 'pam': phase['pam'], 'pfd_pulse': max(store['pfd_pulse']),
                            'dt_pulse': max(store['dt_pulse'])}))
        print("Fused phases '{0}' to combined phase {1}.".format("', '".join(pids), new_pid))

        if purge:
            for pid in set(pids):
                self.delete(pid)
            print("Deleted initial phases: '{0}'.".format("', '".join(pids)))

        return new_pid

    def spectra(self, pid, spectrum, purge=True):
        """
        Maps a spectrum onto a phase. The key 't_actinic' now contains the overall photon flux density while the keys
        named after to the spectrum dictionary keys contain that particular wavelength/color intensity.

        Parameters
        ----------
        pid: str
            Phase ID.

        spectrum: dict
            Dictionary using wavelengths/colors as keys and intensities as values.

        purge: bool
            If True, the initial phase will be deleted afterwards.

        Returns
        -------
        str
            Phase ID of the fused phase.
        """
        if not (isinstance(spectrum, dict) and all(isinstance(v, Number) for v in spectrum.values())):
            raise ValueError("'spectra' expects a dictionary with numeric values!")

        try:
            phase = getattr(self, pid)
        except ModelError:
            raise ModelError(str(pid) + ' is not a valid phase ID!', who=self.spectra)

        # Adding a spectral phase to phases
        new_pid = gibberish(5, self.pids)  # random name as phase ID
        spectral = Phase(new_pid, phase['t_actinic'], phase['pfds_actinic'], phase['parameters'])
        max_actinic_pfd = np.max(phase['pfds_actinic'])
        for w, i in spectrum.items():
            spectra = normalise(phase['pfds_actinic'], [0, i / (phase['parameters']['pfd_pulse'] / max_actinic_pfd)])
            spectra = Phase(None, phase['t_actinic'], spectra, {'pfd_pulse': i})
            setattr(spectral, w, spectra['pfds'])

        if purge:
            self.delete(pid)
            print("Delete original phase: '{0}'".format(pid))

        print("Transformed phase '{0}' to spectral phase {1}.".format(pid, new_pid))
        self.phases.append(spectral)
        return new_pid

    def merge(self, pids, labels, color_model_max=255, color_model_int=True, purge=True):
        # FIXME: merge adds a lot of t=0 and a weird (pfd_start+pfd_later)/2 time at that point.
        """
        Merges several phases into one while treating each phase as different light color. The new phase's tfls contains
        all times at which the light changes. pfds represents the overall light intensity and colors contains tuples of
        single color or color range, respectively, values. The color model is chosen dynamically. If the purge flag is
        set, the initial phases will be deleted after the merge.

        Parameters
        ----------
        pids: Iterable of str
            Phase IDs to be merged.

        labels: Iterable of str
            Labels for the light color

        color_model_max: Number
            Maximal value for the used color model. RGB uses 255 (bytes) most other models use 100 (percent).

        color_model_int: bool
            If True, color_model uses integers (required for some color models, e.g. RGB).

        purge: bool
            If True, the initial phases will be deleted afterwards.

        Returns
        -------
        str
            Phase ID of the fused phase.
        """
        # checking parameters that wouldn't throw exceptions or would throw them way to late
        if not (len(pids) == len(labels)and all([isinstance(l, str) for l in labels])):
            raise ValueError("Dimensions mismatch! Each phase ID needs an unique label!")
        if not isinstance(color_model_max, Number) or color_model_max <= 0:
            raise ValueError("'color_model_max' expected Number greater 0 got '{0}' (type: {1}) instead.".
                             format(str(color_model_max), type(color_model_max).__name__))

        try:
            phase = {'t_actinic': np.array([]), 'pam': np.array([])}
            store = {'length': [], 'dt_pulse': [], 'pfd_pulse': [], 'pfds_actinic': [], 'pfds': []}

            # getting phases and last trait that needs special evaluation (keeping loop short, therefore)
            for pid in pids:
                phase[pid] = getattr(self, pid)
                store['length'].append(phase[pid]['parameters']['length'])
            if not np.allclose(store['length'][0], store['length'][1:]):
                raise ModelError(who='length')

        except ModelError as err:
            msg = {self.__getattr__: 'Expected a list of valid phase IDs. Got {0} instead'.format(str(pids)),
                   'length': "Phases with different lengths can't be merged!"}
            raise ModelError(msg[err.who], who=self.merge)

        # actually working with phases: 1) extracting parameters, 2) merging, 3) deleting duplicates
        for pid in pids:
            store['dt_pulse'].append(phase[pid]['parameters']['dt_pulse'])
            store['pfd_pulse'].append(phase[pid]['parameters']['pfd_pulse'])
            store['light_step'].append(phase[pid]['parameters']['light_step'])
            pos = np.searchsorted(phase['t_actinic'], phase[pid]['t_actinic'])
            phase['t_actinic'] = np.insert(phase['t_actinic'], pos, phase[pid]['t_actinic'])
            phase['pam'] = np.insert(phase['pam'], pos, phase[pid]['pam'])
        pos = np.where(np.isclose(phase['t_actinic'][1:], phase['t_actinic'][:-1]))
        phase['pam'] = np.delete(phase['pam'], pos)
        phase['t_actinic'] = np.delete(phase['t_actinic'], pos)
        param = {'length': store['length'][0], 'pam': phase['pam'], 'pfd_pulse': max(store['pfd_pulse']),
                 'dt_pulse': max(store['dt_pulse']), 'light_step': min(store['light_step'])}

        # adding intermediate light intensities at positions intermediate tfls were added
        for pid in pids:
            ind = np.where(np.array(
                    [not any(np.isclose(t, phase[pid]['t_actinic'], 0.001)) for t in phase['t_actinic']]))
            pos = np.searchsorted(phase[pid]['t_actinic'], phase['t_actinic'][ind])
            pac = (phase[pid]['pfds_actinic'][pos] + phase[pid]['pfds_actinic'][pos - 1]) / 2
            obj = Phase(None, phase['t_actinic'], np.insert(phase[pid]['pfds_actinic'], pos, pac), param)
            store['pfds_actinic'].append(obj['pfds_actinic'])
            store['pfds'].append(obj['pfds'])

        # storing overall pfd and scaling labeled pfds according to colour model
        phase['pfds_actinic'] = np.max(np.vstack(store['pfds_actinic']), 0)
        phase['colors'] = np.array(store['pfds']) / param['pfd_pulse'] * color_model_max
        if color_model_int:
            phase['colors'] = [np.int_(np.round(color))for color in phase['colors']]

        # remove all redundant points
        t_actinic, pfds_actinic = remove_redundancy(phase['t_actinic'], phase['pfds_actinic'], param['light_step'])

        # Adding a merged phase to phases
        new_pid = gibberish(5, self.pids)  # random name as phase ID
        merged = Phase(new_pid, t_actinic, pfds_actinic, param)
        for label, color in zip(labels, phase['colors']):
            setattr(merged, label, color)
        self.phases.append(merged)
        print("Merged phases '{0}' to colored phase {1}.".format("', '".join(pids), new_pid))

        if purge:
            for pid in set(pids):
                self.delete(pid)
            print("Deleted initial phases: '{0}'.".format("', '".join(pids)))

        return new_pid

    # Two different approaches of plotting the light protocol the way it is passed to simulate.integrate (!)
        # Fixme: Results has a function for that. At least comment is obsolete. Also check if these assume _const lfn
    def plot_intensity(self, pids=None):
        """
        Plots the light protocol for the selected phases in order of the phase IDs.

        Parameters
        ----------
        pids: None or Iterable of str
            Phase IDs in desired order. For best results use a iterable that stores order!
        """
        if pids is None:
            print('Using full protocol.')
            pids = self.pids
        ph = [getattr(self, pid) for pid in pids]

        # dummy for ph[i + 1]['pfds'][0] in next loop; two copies of last pfd because one is removed with leading 0 tfl
        ph.append(Phase(None, np.array([0, 0]), np.array([ph[-1]['pfds'][-1]] * 2),
                        {'pam': np.array([]), 'dt_pulse': ph[-1]['parameters']['dt_pulse'],
                         'pfd_pulse': ph[-1]['parameters']['pfd_pulse']}))

        l = np.array([0])  # length correction factor for first phase
        phases = []
        for i in range(len(ph) - 1):
            l = np.hstack([l, ph[i]['parameters']['length']])
            phases.append({'tfls': np.hstack([ph[i]['tfls'], ph[i]['parameters']['length']]),
                           'pfds': np.hstack([ph[i]['pfds'], ph[i + 1]['pfds'][0]])})

        l = iter([sum(l[0:i]) for i in range(1, len(l) + 1)])
        c = rainbow(len(pids))
        for ph in phases:
            plt.plot(ph['tfls'] + next(l), ph['pfds'], 'x:', color=next(c), linewidth=2, ms=8, mew=3)

    def plot_color(self, pids=None):
        """
        Plots the light protocol for the selected phases in order of the phase IDs.

        Parameters
        ----------
        pids: None or Iterable of str
            Phase IDs in desired order. For best results use a iterable that stores order!
        """
        if pids is None:
            print('Using full protocol.')
            pids = self.pids
        phases = [getattr(self, pid) for pid in pids]

        # length correction factor for first phase
        lengths = np.hstack([0, [phase['parameters']['length'] for phase in phases]])
        lengths = [sum(lengths[0:i]) for i in range(1, len(lengths) + 1)]

        for phase, length in zip(phases, lengths):
            plt.plot(phase['tfls'] + length, phase['pfds'], 'x:', color='0.25', linewidth=2, ms=8, mew=3)
            labels = phase.__dict__.copy()
            for key in ['pfds_actinic', 'id', 't_actinic', 'parameters']:
                labels.__delitem__(key)
            mplc = [*cls.cnames.keys(), *cls.cnames.values()]
            colors = labels if all([label in mplc for label in labels]) else rainbow(len(labels))
            for label, color in zip(labels, colors):
                plt.plot(phase['tfls'] + length, phase[label], 'x:', color=color, linewidth=2, ms=8, mew=3)

        if len(phases) > 1:
            for line in lengths[1:]:
                plt.plot([line] * 2, plt.axis()[2:], 'k-', linewidth=2)


class Phase:
    """
    Phase is a storage object for light protocol slices. Phases stores the building blocks of phases and assembles
    them dynamically. Phases are created by the methods of the superclass LightProtocol and are not intended to be
    created manually.
    """
    def __init__(self, pid, t_actinic, pfds_actinic, parameters):
        self.id = pid
        self.pfds_actinic = pfds_actinic
        self.t_actinic = t_actinic
        self.parameters = parameters

    def __call__(self):
        t_pulses = self.parameters['pam'] + self.parameters['dt_pulse']
        t_actinic = self.t_actinic.copy()
        pfds_actinic = self.pfds_actinic.copy()

        tolerance = 0.00000001  # similar tolerance to default of np.isclose

        for p_start, p_stop in zip(self.parameters['pam'] - tolerance, t_pulses + tolerance):
            x = np.where((t_actinic >= p_start) & (t_actinic <= p_stop))
            t_actinic = np.delete(t_actinic, x)
            pfds_actinic = np.delete(pfds_actinic, x)

        # inserting last actinic point prior to pulse of light
        pos = np.searchsorted(t_actinic, self.parameters['pam'])
        t_actinic = np.insert(t_actinic, pos, self.parameters['pam'])
        pfds_actinic = np.insert(pfds_actinic, pos, (pfds_actinic[pos] + pfds_actinic[pos - 1]) / 2)
        # although with removing t0 it might be fixed in some cases.

        # inserting end of light pulse
        pos = np.searchsorted(t_actinic, t_pulses)
        tfls = np.insert(t_actinic, pos, t_pulses)
        pfds = np.insert(pfds_actinic, pos, [self.parameters['pfd_pulse']] * len(t_pulses))

        # if present, remove leading zero
        if tfls[0] == 0:
            tfls, pfds = tfls[1:], pfds[1:]

        return {'tfls': tfls, 'pfds': pfds}

    def __getattr__(self, item):
        if item is 'lightprotocol':
            return self()

        if item in ['tfls', 'pfds']:
            return self()[item]

        if item is 't_pulses':
            return self.parameters['pam'] + self.parameters['pfd_pulse']

        if item is 'pfds_pulses':
            return np.ones(len(self.parameters['pam'])) * self.parameters['pfd_pulse']

        if item in self.parameters:
            return self.parameters[item]

        raise AttributeError("'Phase' object has no attribute/key '{0}'.".format(item))

    def __getitem__(self, key):
        # just enable subscripts; redirects to self.__getattr__
        return getattr(self, key)


class LightDict(dict):
    """
    Storage object for light protocol parameters, derived from build-in dictionary type.
    LightDict performs a validation of every stored light protocol parameter during initiation and every time a light
    protocol parameter is edited. Light protocol parameters that do not pass this validation are discarded and replaced
    by fail save default parameters.

    Note, that for any instance ld of Lightdict, ld['pam'] returns a processed PAM protocol while ld.get('pam') and
    other methods return the stored pam protocol which either can be an array or a code string.

    Table of accepted parameters
    ----------------------------------------------------------------------------------------------------------------
        parameter name      expected type   Description
    ----------------------------------------------------------------------------------------------------------------
            'pam'           str             Gives the times a pulse of light is applied.
                            several         Strings with the pattern delay_interval describe a regular pattern with
                                numbers     or without delay of x seconds and are used to calculate an array of
                                            absolute times as it can also be passed directly by the user for all
                                            kinds of irregular patterns. Empty containers or the string 'off' switch
                                            pam off.

            'length'        number          Represents the total time of a phase in seconds.

            'pfd_start'     number          Photon flux density (light intensity) at the start of a phase.

            'pfd_later'     number          Maximum or minimum PFD that can be reached in the course of the
                                            protocol. Required for 'sine', 'linear' and 'sigmoid' protocols. Ignored
                                            for 'constant' and 'diurnal' protocols.

            'pfd_pulse'     number          Intensity of the PAM light pulse in µE. Expects a positive value. For
                                            plants, usually values around 5000 are used.

            'dt_light'      number          Length of a step of a step in the actinic light in seconds. Expects a
                                            positive value.

            'light_step'    number          Step size for light intensity. Intermediate points are removed. Set to high
                                            values for step function like light protocols.

            'dt_pulse'      number          Length of the PAM light pulse in seconds. Expects a positive value.

            'period'        number         Period of a sine, defines the 'sine' protocol.
                                            If period is greater than 0 and smaller than 1 the graph stretches.
                                            If period is greater than 1 the graph squeezes. If period is equal 0 the
                                            graph would be similar to the 'constant' protocol. Therefore this is not
                                            accepted.

            'offset'        number          Offset of a sine; defines the 'sine' protocol.
                                            If offset is greater 0 it will shift the sine to the left.

            'delay'         number          Time delay, after which a gradient is created; defines both, the
                                            'linear' and 'sigmoid' protocol. If delay is a positive value, the light
                                            remains at the 'pfd_start' for that long and then starts to increase or
                                            decrease towards 'pfd_later'.

            'modifier'      number          Modifier for the slope of sigmoid gradients; defines the 'sigmoid'
                                            protocol. If this is 0, a sigmoid curve will collapse to a linear curve.

            'day_length'    number          Length of a day in a diurnal protocol; defines the 'diurnal' protocol.
                                            Note, that day_length allows days to be less or more than 24 hours long,
                                            if 'length' isn't a product if days times 24. This features was
                                            implemented to simulate extraterrestial days or the simple fact that
                                            many organisms don't realise if an artificial day is less than 24 hours
                                            long and scientist like to cheat with the day lengths anyway. I'm not
                                            that sure about plants but it works really well in chicken farming!

            'sun'           iterable        Contains the times for sunrise and sunset for the 'diurnal' protocol.
                                of tuples   Each tuple consists of two floats giving the sunrise and sunset times in
                                            seconds. Sun needs to have as many tuples as days are possible by
                                            'day_length' subdividing 'length'. If only one tuple is given it will be
                                            interpretated as "Always use the same sunrise/sunset times".
    """

    # Set of fail-save default parameters
    default_parameters = {'pam': np.array([0, 60, 120, 180, 240]), 'length': 300, 'pfd_start': 100, 'pfd_later': 100,
                          'pfd_pulse': 5000, 'dt_light': 1.0, 'light_step': 1.0, 'dt_pulse': 1.0, 'delay': 0,
                          'modifier': 0, 'period': 0, 'offset': 0, 'day_length': None,
                          'sun': [np.array([28800, 72000])]}

    def __init__(self, **parameters):
        """
        Initiates LightDict dictionary. The passed parameters are validated by check and then passed together with fail
        save default parameters to super class dict.

        Parameters
        ----------
        parameters
            Parameters as keyword arguments. Accepts:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                light_step   number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs
        """
        super().__init__({**LightDict.default_parameters, **self.check(**parameters)})

    def __setitem__(self, key, parameter):
        """
        Sets an item in super class dict if key and parameters pass the check function.
        This method is equivalent to assigning to a subscript of an LightDict instance:
            ld.__setitem__(key, parameter) <==> ld[key] = parameter

        Parameters
        ----------
        key: str
            Key of item in LightDict. Accepted keys are:
            'pam', 'length', 'pfd_start', 'pfd_later', 'pfd_pulse', 'dt_light', 'light_step', 'dt_pulse', 'delay',
            'modifier', 'period', 'offset', 'day_length', 'sun'

        parameter
            Parameter according to keyword. Accepts:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                light_step   number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs
        """
        valid = self.check(**{key: parameter})
        if valid:
            super(LightDict, self).__setitem__(key, valid[key])
        else:
            print('No parameter was changed.')

    def __getitem__(self, key):
        """
        Gets an item by its key from LightDict. If the key is 'pam' the PAM protocol is parsed otherwise the call is
        redirected to super class dict. This methods is equivalent to passing key as subscript:
            ld.__getitem__(key) <==> ld[key]

        Parameters
        ----------
        key: str
            Key of item in LightDict. Accepted keys are:
            'pam', 'length', 'pfd_start', 'pfd_later', 'pfd_pulse', 'dt_light', 'light_step', 'dt_pulse', 'delay',
            'modifier', 'period', 'offset', 'day_length', 'sun'

        Returns
        -------
            Item stored under this key.

        """
        item = super(LightDict, self).__getitem__(key)
        if key is 'pam':
            if isinstance(item, str):
                item = [0, 0] if item is 'off' else [int(n) for n in re.findall('\d+', item)]
                if len(item) == 2 and item[1] > 0:
                    item = np.arange(item[0], self['length'], item[1])
                    item = np.delete(item, np.where(item + self['dt_pulse'] > self['length']))
                else:
                    item = np.array([])
        elif key is 'sun':
            item = item if item is not None else self['length']
        return item

    @classmethod
    def check(cls, **parameters):
        """
        Checks if passed keyword arguments pass as parameters and replaces them with default values if not. Whenever a
        non-valid parameter is replaced a message is printed to the console. The final parameter dictionary is returned
        but not completed with default values, as this interferes with the protocol determination method of
        LightProtocol.

        Parameters
        ----------
        parameters
            Parameters as keyword arguments. Accepts:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                light_step   number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs

        Returns
        -------
        dict
            kwargs with invalid elements replaced by default values.
        """
        m = []  # Collects error messages

        # Discarding unknown parameters
        full_set = set(cls.default_parameters)
        if not set(parameters).issubset(full_set):
            unknown = set(parameters).difference(full_set)
            for k in unknown:
                parameters.pop(k)
            m.append(" Discarded unknown parameters: '{0}'".format("', '".join(unknown)))

        # Rules for parameter validation
        # for the less complex rules anonymous functions bound to the parameters keys. for more complicated rules like
        # for PAM protocols a function is defined and bound to a parameter key.
        def lambda_pam(x):
            if isinstance(x, str):
                if not(x is 'off' or len([int(n) for n in re.findall('\d+', x)]) == 2):
                    return False
            else:
                try:
                    x = list(x)  # provokes exception: TypeError
                    x.sort()
                    x = [float(t) for t in x]  # provokes exception: ValueError or TypeError
                    l = parameters['length'] if 'length' in parameters else cls.default_parameters['length']
                    dt = parameters['dt_pulse'] if 'dt_pulse' in parameters else cls.default_parameters['dt_pulse']
                    x.append(l)  # last point must be smaller than phase total length
                    if any([x[i-1] + dt >= x[i] for i in range(1, len(x))]):
                        raise ValueError
                    parameters['pam'] = np.array(x[:-1])
                except (ValueError, TypeError):
                    return False
            return True

        def lambda_sun(x):
            l = parameters['length'] if 'length' in parameters else cls.default_parameters['length']
            dl = parameters['day_length'] if 'day_length' in parameters else cls.default_parameters['day_length']
            try:  # List comprehension provokes an exception if sun does not have a very specific structure
                if all(isinstance(e, Number) for e in x) and len(x) == 2 and 0 <= x < x <= dl:
                    parameters['sun'] = [np.array(x)]  # same format as the misc.sunrise_equation output format
                elif not (np.isclose(len(x) * dl, l) and all([0 <= r < s <= dl for r, s in x])):
                    raise ValueError
                return True
            except (ValueError, TypeError):
                return False

        rules = OrderedDict()
        rules['length'] = lambda x: isinstance(x, Number) and x > 0
        rules['pfd_start'] = lambda x: isinstance(x, Number) and x >= 0
        rules['pfd_later'] = lambda x: isinstance(x, Number) and x >= 0
        rules['pfd_pulse'] = lambda x: isinstance(x, Number) and x >= 0
        rules['dt_light'] = lambda x: isinstance(x, Number) and x > 0
        rules['light_step'] = lambda x: isinstance(x, Number) and x > 0
        rules['dt_pulse'] = lambda x: isinstance(x, Number) and x > 0
        rules['pam'] = lambda_pam
        rules['delay'] = lambda x: isinstance(x, Number) and 0 <= x <= parameters['length'] \
            if 'length' in parameters else cls.default_parameters['length']
        rules['modifier'] = lambda x: isinstance(x, Number) and -1 < x < 1
        rules['period'] = lambda x: isinstance(x, Number)
        rules['offset'] = lambda x: isinstance(x, Number)
        rules['day_length'] = lambda x: (isinstance(x, Number) and x > 0) or x is None
        rules['sun'] = lambda_sun

        # looping over numerical non-default parameters and processing them
        temp = parameters.copy()
        for k, p in temp.items():
            if not rules[k](p):
                parameters.pop(k)
                m.append("'{0}': Parameter does not pass requirements and will be discarded.".format(k))

        if m:  # m yields True, if m is not empty
            print('\n'.join(m))
        return parameters

    @classmethod
    def set_default_parameters(cls, **parameters):
        """
        Overwrites the fail save default parameters with own default parameters providing that they pass a validation.

        Parameters
        ----------
        parameters
            Parameters as keyword arguments. Accepts:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                light_step   number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs
        """
        cls.default_parameters = {**cls.default_parameters, **cls.check(**parameters)}

    @staticmethod
    def protocol(**parameters):
        """
        Tries to guess the protocol from the passed parameter dictionary, according to the table below.
        Please note, that protocol is a static method, hence it will return the protocol for the default parameters if
        not provided with other parameters.

            --------------------------------------------------------------------------------------------------------
            Protocols   Short description
            --------------------------------------------------------------------------------------------------------
            constant    Constant light at given light intensity (PFD value).

            sine        Light intensity fluctuates with a sine curve between two given PFD values.

            linear      Linear rising or falling light intensity between two given PFD values.

            sigmoid     Light intensity changes with a sigmoid (s-shaped) curve between two given PFD values.

            diurnal     Simulates the course of N days. With rising light intensity from sunrise to midday, then
                        falling light intensity and finally darkness until it is sunrise.

            --------------------------------------------------------------------------------------------------------
            Note:   The parameter light_step can be used to create step functions with linear and sigmoid protocols.
                    Althought, a sigmoid step function might look weird.
            --------------------------------------------------------------------------------------------------------

            Table of protocol defining parameters.
                    x marks parameters which define a protocol.

                        constant, linear, sigmoid, sine, diurnal
            length          o       o       o       o       o
            pfd_start       x       o       o       o       .
            pfd_later       x       o       o       o       o
            pfd_pulse       o       o       o       o       o
            dt_light        o       o       o       o       o
            light_step      o       o       o       o       o
            dt_pulse        o       o       o       o       o
            delay           .       o       o       .       .
            modifier        .       .       x       .       .
            period          .       .       .       x       .
            offset          .       .       .       o       .
            day_length      .       .       .       .       x
            sun             .       .       .       .       o
            pam             o       o       o       o       o

        Parameters
        ----------
        parameters
            Parameters as keyword arguments. It's is however highly recommended to pass the output of the method
            parameters instead of passing an unvalidated parameter dictionary to this method. Recommended:

                pam          str ('off', 'XX_YY' [where XX/YY are integers]) or iterable of integers < length
                length       number > 0
                pfd_start    number >= 0
                pfd_later    number >= 0
                pfd_pulse    number >= 0
                dt_light     number > 0
                light_step   number > 0
                dt_pulse     number > 0
                delay        number, 0 <= x <= length
                modifier     number, -1 < x < 1
                period       number
                offset       number
                day_length   number > 0 or None
                sun          tuple or list of np.arrays with sunrise and sunset value pairs

        Returns
        -------
        str
            Name of a matching protocol.
        """
        pfd_start = parameters.get('pfd_start', LightDict.default_parameters['pfd_start'])
        pfd_later = parameters.get('pfd_later', LightDict.default_parameters['pfd_later'])
        period = parameters.get('period', LightDict.default_parameters['period'])
        day_length = parameters.get('day_length', LightDict.default_parameters['day_length'])
        modifier = parameters.get('modifier', LightDict.default_parameters['modifier'])

        if pfd_start == pfd_later:
            protocol = 'diurnal' if day_length else 'constant'
        else:
            protocol = 'sine' if period > 0 else 'sigmoid' if modifier else 'linear'

        return protocol


def remove_redundancy(t, x, tolerance=1.0):
    """
    Takes two vectors t and x(t) and removes all points in t and x that are redundant in x(t). A point is classified as
    redundant if it's not the last point in a block of points that are close to each other within the given tolerance.

    Parameters
    ----------
    t: np.array
        Passive vector (here t depends on x, even if x normally would depend on t). Needs to have the same length as x

    x: np.array
        Active vector. Based in this vector, the function decides which points in t and x are redundant.

    tolerance: positive Number
        Tolerance used to identify coherent blocks of similar values.

    Returns
    -------
    np.array, np.array
        t and x with all redundant values removed.
    """
    i = 0
    while i < len(x):
        p = np.where(np.isclose(x, x[i], atol=tolerance))[0]
        e = np.append(p[np.where(p[1:] - p[:-1] > 1)], p[-1])  # essential points
        ne = list(set(p).difference(e))  # non-essential points
        t, x = np.delete(t, ne), np.delete(x, ne)
        i += 1
    return t, x
