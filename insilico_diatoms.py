#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from externals.duration import Duration
from externals.colors import rainbow
import lightprotocol
import lightfunction
import matplotlib.pyplot as plt
import model  # Not unsused (!), just called implicitly!
from multiprocess.pool import Pool
import numpy as np
import parameters
import simulate
from time import time

__author__ = "Philipp Norf"
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"

start = time()  # just out of curiosity.
d = Duration()  # allows human readable time formats


def simulation(species, model_version, intensity, param):

    # Setting up model parameters
    p = parameters.Parameters(species)
    for k, v in param.items():
        p.edit(**{k: v})
    p.process()
    y0 = np.array([p.PQtot, 0.0202, 5.000, 0.0000, 0, 0.0000, 0.0001, 1, 0.0000])

    # rec-reating light protocol according to Goss et al. 2006
    # (Title: The importance of a highly active and delta pH dependent...)
    #   * Simulate with 60 seconds interval
    #   * 15 min high light (800 µE)
    #   * 30 min darkness or low light (20 µE)
    #   * Different experiments with inhibitors added at phase start (DEP inhibitor: DTT)
    l = lightprotocol.LightProtocol(pam='0_60', pfd_pulse=5000, dt_pulse=0.7, light_step=10)
    l.create(length=d.m15, pfd_start=800, pfd_later=800)
    l.create(length=d.m30, pfd_start=intensity, pfd_later=intensity)
    lfn = lightfunction.LightFunction(l, '_constant')  # storing light protocol results in two light functions

    # Running the simulation
    m = eval('model.' + model_version + '(p)')
    s = simulate.Simulate(m, lfn, y0)
    s.piecewise_constant()
    s.finish()

    # Extracting and preparing results for return (can't return an object and can't plot)
    e = {'species': species, 'model': model_version, 'intensity': intensity}
    e['t_Fluo'], e['Fluo'], e['Fmax'], e['Tm'], e['Fm'], e['Ts'], e['Fs'], _, _ = s.get_fluo()
    e['t'] = s.get_t()
    e['deep'] = s.get_rate('deep')
    e['epox'] = s.get_rate('epox')

    for rate in ['ps2', 'ps1', 'ptox', 'cytb6f', 'ndh', 'fnr', 'cyc', 'ATPsynthase', 'ATPconsumption',
                 'NADPHconsumption', 'leak', 'st12', 'st21', 'deep', 'epox', 'cs2', 'ATPactivity']:
        e[rate] = s.get_rate(rate)

    for var in s.model._name:
        e[var.name] = s.get_var(var)

    return e


pool = Pool()
stars = [
    ('default', 'LightModel', 0, {'swATPsynthase': -2, 'kepox': 0.004}),
    ('default', 'LightModel', 20, {'swATPsynthase': -2, 'kepox': 0.004}),
    ('diatom', 'DiatomModel', 0, {'swATPsynthase': -2, 'kepox': 0.004}),
    ('diatom', 'DiatomModel', 20, {'swATPsynthase': -2, 'kepox': 0.004})
]
experiments = pool.starmap(simulation, stars)
figures = []

c = rainbow(len(stars)+1)
figures.append(plt.figure())
for e in experiments:
    col = next(c)
    plt.plot(e['Tm'], (e['Fmax'] - e['Fm'])/e['Fmax'], '.-', color=col)
    plt.xlabel('Time [s]')
    plt.ylabel('Normalised fluorescence')


f, a = plt.subplots(2, 1, sharex=True, sharey=True, squeeze=False)
x = experiments
c = rainbow(3)
e, d = next(c), next(c)
a[0, 0].plot(x[0]['Tm'], (x[0]['Fmax'] - x[0]['Fm'])/x[0]['Fmax'], '+-', color=d)
a[0, 0].plot(x[1]['Tm'], (x[0]['Fmax'] - x[1]['Fm'])/x[0]['Fmax'], '+-', color=e)
a[1, 0].plot(x[2]['Tm'], (x[0]['Fmax'] - x[2]['Fm'])/x[0]['Fmax'], '+-', color=d)
a[1, 0].plot(x[3]['Tm'], (x[0]['Fmax'] - x[3]['Fm'])/x[0]['Fmax'], '+-', color=e)
a[1, 0].plot(x[3]['Tm'], (x[0]['Fmax'] - x[3]['Fm'])/x[0]['Fmax'], 'x-', color=e)
a[1, 0].plot(x[2]['Tm'], (x[0]['Fmax'] - x[2]['Fm'])/x[0]['Fmax'], 'x-', color=d)
a[0, 0].plot(x[1]['Tm'], (x[0]['Fmax'] - x[1]['Fm'])/x[0]['Fmax'], 'x-', color=e)
a[0, 0].plot(x[0]['Tm'], (x[0]['Fmax'] - x[0]['Fm'])/x[0]['Fmax'], 'x-', color=d)
f.text(0.04, 0.5, 'Non-photochemical quenching', va='center', rotation='vertical')
f.text(0.5, 0.04, 'Time [s]', ha='center')
from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('insilico_diatom_fluo.pdf')
pp.savefig(f)
pp.close()
plt.show()
