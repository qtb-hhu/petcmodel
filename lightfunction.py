#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

# TODO: method to manually save set/empty all counters / miscellaneous light function parameters (i, _i_fn, _tpid)

# TODO: Testing.    [v] checks out ok; [x] has issues; [_] untested
# state light function          Comment
#  [v]  _constant               works.
#  [v]  _timedependent          works with _constant
#  [x]  _linear                 error:  lsoda--  warning..internal t (=r1) and h (=r2) are
#                                           such that in the machine, t + h = t on the next step
#                                   occurs at first light flash
#  [x]  _careful                error:  call to LightProtocol.get_pid_from_t(t): t is out of range of tfls.
#                                   method might be faulty, after last changes. Test it manually.

#  [_]  _phasedependent         LightProtocol.get_pid_from_t was faulty before. Fix _careful error first


"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from externals.norm_tools import crop
from miscellaneous import ModelError
from numpy import inf, polyfit, isclose, hstack, where, array
import re

__author__ = "Philipp Norf"
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class LightFunction:
    """
    Contains the light functions used by the integrator.

    Please note, that the scope for passing a custom light function as argument is very limited. Instead it is
    recommended to add custom light functions by inheriting the LightFunction class and writing them as private methods
    (starting with an underscore) of the child class.

    Light functions need to take a single time and return a single photon flux density. To keep integration time low, it
    is recommended to avoid extensive calculations and lookups in light functions. Best practice calls for calculating
    as much as possible beforehand.
    """
    def __init__(self, lightprotocol, light_fn, *light_fns, pids=(), t0=0, fail_save_mode=None):

        # Setting counters
        self.i = 0  # pointer for current iteration of integration. Equivalent to current index of photon flux density.
        self._i_fn = 0  # index of current subfunction of light function
        self._tpid = array([0])

        # Setting up the light protocol
        self.l = None
        self.pids = pids
        self.tfls = array([])
        self.pfds = array([])

        self._light_fn = self._default
        self._light_fns = []
        self.set_lightprotocol(lightprotocol, *pids, t0=t0)
        self.set_light_fn(light_fn, *light_fns)

        # Configuration flags
        self.set_fail_save_mode(fail_save_mode)

    def __call__(self, t):
        """
        The LightFunction instance returns the current photon flux density if called like a function with a time point
        as parameter. Internally this redirects to calling the function reference stored in the _light_fn attribute.

        Parameters
        ----------
        t: float
            Time point of interest.

        Returns
        -------
        float
            Photon flux density at time point t.
        """
        return self._light_fn(t)

    def map(self, t, function, keyword, *args, **kwargs):
        """
        Maps a function to the return value (photon flux density) of the light function.
        The photon flux density is passed as a keyword parameter. Using the string in keyword as keyword parameter.
        Additional parameters can be specified by using *args and **kwargs.

        Parameters
        ----------
        t: float
            Time in seconds.

        function: function
            Reference to the function that os mapped to the photon flux density.

        keyword: str
            Keyword the photon flux density. This overwrites any keyword conflicts with kwargs.

        args: *args
            Additional parameters for function.

        kwargs: **kwargs
            Additional keyword parameters for function.

        Returns
        -------
        Return value(s) of the function that is mapped on the return value (photon flux density) of the light function.
        """
        kwargs[keyword] = self.set_light_fn(t)
        return function(*args, **kwargs)

    def set_counter(self, i):
        """
        Method to be called by the integration method used to set the next light function or photon flux density.
        Values that exceed the total number of time points in a light protocol are cropped to the maximum value.

        Parameters
        ----------
        i: int
            New position of the counter.
        """
        self.i = crop(i, (0, len(self.pfds)), int)

    def increment_counter(self):
        """
        Increments the photon flux density counter by one.
        """
        self.i += 1

    def set_lightprotocol(self, lightprotocol, *pids, t0=0):
        """
        Sets the light protocol and adjusts all other attributes that depend on the light protocol.

        Parameters
        ----------
        lightprotocol: LightProtocol
            Instance of LightProtocol class.

        pids: str
            Phase IDs for phases to be set as photon flux densities.

        t0: Number
            Time at which the light protocol starts. Required for changing a light protocol on the fly.
        """
        try:
            if not set(pids).issubset(lightprotocol.pids):
                raise KeyError

        except (TypeError, KeyError):
            raise ModelError("'lightprotocol' expects an instance of class LightProtocol while pids expects a list of "
                             "valid phase IDs or nothing. Got type '{}' for lightprotocol and type '{}' for pids."
                             .format(type(lightprotocol).__name__, type(pids).__name__))
        else:
            # Setting attributes
            setattr(self, 'l', lightprotocol)
            self.set_phases(*pids, t0=t0)

            # Adjusting existing configurations
            self.set_counter(crop(self.i, (0, len(self.pfds)), int))
            if (self._light_fn.__code__.co_names[0] == '_timedependent' and self._light_fns[-1][0] > self.tfls[-1]) or\
               (self._light_fn.__code__.co_names[0] == '_phasedependent' and len(self.l.pids) != len(self._light_fns)):
                    self.set_light_fn('_default')

    def set_phases(self, *pids, t0=0):
        """
        Method to set the phases used by the light protocol to calculate the full protocols time and photon flux density
        vectors.

        Parameters
        ----------
        pids: str
            Phase IDs for phases to be set as photon flux densities.

        t0: Number
            Time at which the light protocol starts.
        """
        try:
            tfls = self.l[pids]['tfls'] + t0
            pfds = self.l[pids]['pfds']

        except AttributeError:
            raise ModelError("Could not find light protocol. Possible not yet set.")

        except KeyError:
            raise ValueError("The passed arguments '{}' are not valid phase IDs."
                             .format("', '".join(str(pid) for pid in set(pids).difference(self.l.pids))))

        except TypeError:
            raise ValueError("'t0' expected type 'Number' got type '{}' instead".format(type(t0).__name__))

        else:
            pids = pids if pids else self.l.pids
            tpid = [self.l[pid].t_actinic[-1] for pid in pids]
            tpid = [sum(tpid[:i]) for i in range(1, len(tpid) + 1)]
            tpid = hstack([tpid, inf])

            setattr(self, '_tpid', tpid)
            setattr(self, 'pids', pids)
            setattr(self, '_pids', hstack([pids, pids[-1]]))
            setattr(self, 'tfls', tfls)
            setattr(self, 'pfds', pfds)

    def set_fail_save_mode(self, light_fn):
        """
        Sets the behaviour of the LightFunction object if the __call__()-method of the instance fails.
        If the failure mode is not set, an error is raised if calling the instance fails.

        Parameters
        ----------
        light_fn: None or str
            Name of the light function that will be used as a default light function or None. Non-existing light
            functions and everything else will set the default light function to always return 0.
        """
        if light_fn is None:
            setattr(self, '_fail_save_mode', None)

        elif light_fn in self.get_light_functions() and light_fn not in ('_timedependent', '_phasedependent'):
            setattr(self, '_fail_save_mode', eval('self.' + light_fn))

        else:
            setattr(self, '_fail_save_mode', True)

    def set_light_fn(self, light_fn, *light_fns):
        """
        Method to evaluate and set the light function.

        Parameters
        ----------
        light_fn: str or function
            Name of a light function as string or reference to the light function itself.
            Note that build in light functions start with an underscore. Use get_light_function(verbose=True) to print
            all available light functions and their documentation.

        light_fns: list if tuples
            List of tuples containing times and light functions or phase IDs and light functions that are to be used if
            the actual light function is '_timedependent' or '_phasedependent'.
        """
        def _eval_fn(fn):
            if fn in buildins:
                return eval('self.' + fn)

            elif callable(fn):
                return fn

            else:
                return self._default

        buildins = self.get_light_functions()
        setattr(self, '_light_fn', _eval_fn(light_fn))
        setattr(self, '_light_fns', [])

        # Removing _timedependent and _phasedependent as recursion is not intended at this point
        buildins.remove('_timedependent')
        buildins.remove('_phasedependent')

        if light_fn is '_timedependent':
            max_t = max(self.tfls)
            try:
                if light_fns[0][0] != 0:
                    setattr(self, '_light_fns', [(0, self._default)])

                self._light_fns.extend([(crop(t, (0, max_t)), _eval_fn(fn)) for t, fn in sorted(light_fns)])
                self._light_fns.append((inf, self._default))  # never used, just here to prevent out of range errors.

            except (ValueError, TypeError):
                raise ValueError("'light_fns' expected list of tuples containing a time and a light function ID.")

        elif light_fn is '_phasedependent':
            pids = self.l.pids
            if len(pids) != len(light_fns):
                raise IndexError("Dimension mismatch: LightProtocol.pids and light_fns do not have the same length.")

            try:  # Nested try blocks so outer else statement is still executed if inner block handles an error.
                try:  # inner try block catches a minor error and lets all mayor exceptions bleed through.
                    if not all(pid in pids for pid, fn in light_fns):
                        raise IndexError
                except ValueError:
                    light_fns = zip(pids, light_fns)

            except IndexError:
                raise ValueError("'light_fns' phase values do not match the light protocols phase IDs.")

            except TypeError:
                raise ValueError("'light_fns' expected a list of tuples containing a phase ID and a light function ID.")

            else:
                setattr(self, '_light_fns', {phase: _eval_fn(fn) for phase, fn in light_fns})

    def get_light_functions(self, verbose=False):
        """
        Returns a list of all known light functions.
        If verbose is set True, get_light_functions prints the documentations of all build in light functions instead.

        Please note, that actual light functions are internal methods of the LightFunction object and should not be
        called directly.

        Returns
        -------
        None or list
            List of all known light functions if not verbose.
        """
        fns = [fn for fn in dir(self) if re.match('^_[^_]', fn) and fn not in ('_light_fn', '_light_fns')]

        if verbose:
            for fn in fns:
                print("Light function {} of {}:\n{}\n".format(fn, self.__class__.__name__, getattr(self, fn).__doc__))
        else:
            return fns

    def _constant(self, t):
        """
        Light function for constant light.
        Returns a constant photon flux density until the counter is increased manually to the next PFD by calling the
        set_counter()-method. This is usually done by the integration method itself.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        return self.pfds[self.i]

    def _linear(self, t):  # Fixme: See _careful, maybe delete.
        """
        Light function for linear interpolated light.
        Assumes the photon flux density between two points to be a linear function and returns a PFD value accordingly
        to the position of t between the two points times.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        return NotImplementedError('Just broken.')
        x = self.tfls[self.i], self.tfls[self.i + 1]
        y = self.pfds[self.i], self.pfds[self.i + 1]
        m, b = polyfit(x, y, 1)
        return t * m + b

    def _careful(self, t):
        # Fixme: Don't like the name.
        # Fixme: t0 at self.i == 0 needs t0 be 0 or -1 or something else smaller than t. Currently it's equal to t1
        # Fixme: same with pfd
        # Fixme: counter needs to start with 1 then.
        # counter is also used externally maybe it's better to remove it there or have a second internal counter
        """
        Light function for carefully linear interpolated light.
        Assumes the photon flux density between two points to be a linear function and returns a PFD value accordingly
        to the position of t between the two time points but watches out for light pulses and does not assume a linear
        function if one of the points is a light pulse.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        pfd_pulse = self.l[self._pids[where(t <= self._tpid)[0][:1]][0]].pfd_pulse
        pfd = array([self.pfds[self.i - 1], self.pfds[self.i]])
        # Fixme: may pick pfds[-1] as t0 for first t to be integrated.

        if all(pfd < pfd_pulse):  # fixme: test. Changed any to all made more sense i think
            m, b = polyfit((self.tfls[self.i], t), pfd, 1)
            return t * m + b

        else:
            return pfd[1]  # returns pfd_pulse if pfd[1] is light pulse and pfd if pfd[0] is light pulse

    def _timedependent(self, t):
        """
        Light function to allow for the use of different light functions at different times in the simulation.
        Each light function is stored with the time at which it replaces the previous light function.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        if t >= self._light_fns[self._i_fn][0]:
            self._i_fn += 1

        return self._light_fns[self._i_fn - 1][1](t)

    def _phasedependent(self, t):
        """
        Not yet implemented.

        Light function to allow for the use of different light function at different phases of the light protocol.
        Each light function is stored with the name of the phase at which it replaces the previous light function.

        Note, that the look up makes this slower than the repetitive light functions.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        if t >= self._tpid[self._i_fn]:
            self._i_fn += 1

        return self._light_fns[self._i_fn - 1][1](t)

    def _default(self, t):
        """
        Light function that is set if no valid light function was available,
        Raises an error by default. Returns always 0 if in debugging mode and redirects to another light function if
        failure_mode specifies one.

        Parameters
        ----------
        t: float
            Time in seconds.

        Returns
        -------
        float
            Photo flux density.
        """
        if self._fail_save_mode is None:
            raise ModelError('Unknown or invalid light function!')

        print('Warning: Unknown or invalid light function. Using default instead!')
        return 0 if self._fail_save_mode is True else self._fail_save_mode(t)
