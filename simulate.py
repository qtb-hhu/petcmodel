#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

# Todo: run .__dict__ keys on simulate and add all other runtime attributes to del statement in Simulate.finish()
# results.Results carries over following attributes: self.model, self.results and self.lfn
# everything else has to be deleted by the finish method.


"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

from math import floor
from miscellaneous import ModelError
import numpy as np
import results
from scipy.integrate import ode

__author__ = ["Anna Matuszyńska", "Philipp Norf"]
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszyńska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class Simulate:
    """ class includes integration methods """

    def __init__(self, model, lightfunction, y0, solver='lsoda', min_step=1e-8, max_step=0.1, nsteps=500):
        """
        Initialises the simulation by setting up the model, the base arguments and the integrator object.

        Parameters
        ----------
        model: model.PETCmodel or similar
            Instance of any *model class from the model module. Provides the ordinary differential equations and rates.

        lightfunction: lightfunction.LightFunction
            Instance of the LightFunction class. Specifies the behaviour of the light during an integration step.

        y0: numpy.array
            Vector of starting values.

        solver: str
            Name of a solver used by the integrator object.


        min_step: float
            Minimum step size for integration.

        max_step: float
            Maximum step size for integration.

        nsteps: int
            Number of steps for integration (?*). [* not sure]
        """
        # setting model
        self.model = model
        self.lfn = lightfunction
        self.y0 = y0
        self.results = []

        # generating integrator object
        self.integrator = ode(self._dydt).set_integrator(solver, max_step=max_step, nsteps=nsteps)
        self.solver = solver
        self.min_step = min_step
        self.max_step = max_step
        self.nsteps = nsteps

        # setting configuration flags
        self._successful = True
        self._monitor = True
        self._warnings = False

    def __call__(self, integration_method='piecewise_constant', *args, **kwargs):
        """
        Enables the start of the simulation by calling the instance of Simulate.

        Parameters
        ----------
        integration_method: str
            Name of the integration method to be used.

        args: *args
            Optional. Arguments for the integration method.

        kwargs: **kwargs
            Optional. Keyword arguments for the integration method.

        Returns
        -------
        Return value of the integration method.
        """
        if integration_method in ['piecewise_constant', 'time_course']:
            return getattr(self, integration_method)(args, kwargs)

    def _dydt(self, t, y):
        """
        System of ordinary differential equations.

        Parameters
        ----------
        t: Number
            Time point.

        y: numpy.array
            Vector of starting values.

        Returns
        -------
        numpy.array
            State of the model at passed time point.
        """
        return self.model.model(y, self.lfn(t))

    def does_monitor(self, set_monitor=None):
        if set_monitor is not None:
            self._monitor = bool(set_monitor)
        return self._monitor

    def reset(self):
        """
        Clears out the results and resets all configuration flags back to their default values.
        """
        self._successful = True
        self._monitor = True
        self._warnings = False
        self.results = []

    def integrate(self, t):
        """
        Integration method, returns state of variables at given time point.

        Parameters
        ----------
        t: Number
            Time point to integrate.

        Returns
        -------
        numpy.array
            State of variables at given time point.
        """
        r = self.integrator
        y0 = r.y
        t0 = r.t

        step = self.max_step
        nsteps = max(self.nsteps, 10*floor((t-t0)/step))

        while step >= self.min_step:
            # suppress FORTRAN warnings
            if not self._warnings:
                r._integrator.iwork[2] = -1

            try:
                r.integrate(t)

                if r.successful():
                    break

            except ModelError:
                print('caught error at ', step, '. Reducing step size')

            step /= 10
            nsteps *= 10

            if self._warnings:
                print('nsteps=', nsteps, ', step=', step)
                print(r.t, r.y)
                print(self.model.rates(r.y, self.lfn(r.t)))

            r.set_integrator(self.solver, max_step=step, nsteps=nsteps)
            r.set_initial_value(y0, t0)

        if step < self.max_step:  # set back to standard steps
            r.set_integrator(self.solver, max_step=self.max_step, nsteps=self.nsteps)

        self._successful = r.successful()
        return r.y

    def time_course(self, t, y0):
        """
        Integration over time.

        Parameters
        ----------
        t: numpy.array
            Vector of times when the light changes.

        y0: numpy.array
            Starting values for integration.

        Returns
        -------
        numpy.array
            State of variables at given time point.
        """
        self._successful = True
        y0s = [y0]
        self.integrator.set_initial_value(y0, t[0])

        i = 1
        while i < len(t) and self._successful:

            if self._warnings:
                print(i, y0s)
                print(t[i])

            y0s.append(self.integrate(t[i]))
            # print('t: ', t[i])
            i += 1

        if self.does_monitor() and self._successful:
            self.results.append({'t': t, 'y': np.vstack(y0s), 'lfn_counter': self.lfn.i})

        return np.vstack(y0s)

    # def phasewise_integration(self, *args, y0=None, nsteps=None):
    #     # Fixme: stops at first light flash because lsoda crashes with message:
    #     #           lsoda--  repeated calls with istate = 1 and tout = t (=r1)
    #     #           in above message,  r1 =  0.9000000000000D+02
    #     #           lsoda--  run aborted.. apparent infinite loop
    #     # likely site of error: LightFunction is initialized with full lightProtocol, not with phases.
    #     """
    #     Integrates over all phases in the same order as they were passed to this method.
    #     the integration runs out after all passed phases have been integrated but will continue were it left off if the
    #     method is called again with new phases and starting values that pick up the previous run. Use this property to
    #     change parameters during the simulation.
    #
    #     Parameters
    #     ----------
    #     *args: lightprotocol.Phase
    #         Light protocol phases to be integrated.
    #
    #     y0: numpy.array or None
    #         Vector of starting values. Used the instance default starting values if set to None.
    #
    #     nsteps: int or None
    #         Number of steps used by the integrator.
    #
    #     Returns
    #     -------
    #     Results of the simulation.
    #     """
    #     return NotImplementedError('This somehow crashes the simulation by generating an infinite loop in lsoda')
    #
    #     ts = np.array([])
    #     try:
    #         t = self.results[-1]['t'][-1]
    #     except (IndexError, KeyError):
    #         t = 0
    #
    #     y0s = np.vstack([self.y0]) if y0 is None else np.vstack([y0])
    #     tfls = np.hstack([phase.tfls for phase in args])
    #     length = len(tfls)
    #
    #     if nsteps is None:
    #         nsteps = np.hstack([100] * length)
    #
    #     while self._successful and self.lfn.i < length:
    #         trange = np.linspace(t, tfls[self.lfn.i], nsteps[self.lfn.i])
    #         self.lfn.increment_counter()
    #         ysim = self.time_course(self.lfn, trange, y0s[-1, :])
    #         y0s = np.vstack([y0s, ysim])
    #         ts = np.hstack([ts, trange])
    #         t = tfls[self.lfn.i]
    #
    #     return ts, y0s

    def piecewise_constant(self, time_steps=100):
        """
        Piecewise integration of constant light.
        The times when the light changes and the photon flux densities of the light are taken from the light protocol
        stored in the LightFunction instance. All other arguments are optional.

        Parameters
        ----------
        time_steps: int
            Number of time steps for each piece of the integration.

        Returns
        -------
        (numpy.array, numpy.array)
            Vector of time points of the integration and  vector of states of variables at these time points.
        """

        y0s = np.vstack([self.y0])
        t = np.array([])
        t0 = 0
        tfls = self.lfn.tfls

        try:
            time_steps = np.array([int(time_steps)] * len(tfls))

        except TypeError:
            raise ValueError("'time_steps' expected type 'int' got type '{}' instead."
                             .format(type(time_steps).__name__))

        while self._successful and self.lfn.i < len(tfls):
            trange = np.linspace(t0, tfls[self.lfn.i], time_steps[self.lfn.i])
            ysim = self.time_course(trange, y0s[-1, :])
            y0s = np.vstack([y0s, ysim])
            t = np.hstack([t, trange])
            t0 = tfls[self.lfn.i]
            self.lfn.increment_counter()

        return t, y0s

    def finish(self, figure=None, axarr=None):
        """
        Finishes up the simulation by checking if values are stored in self.results and if so converting the Simulate
        instance into a Results instance. By overwriting the __class__ attribute and dropping all attributes related to
        integration methods.

        Parameters
        ----------
        figure: figure object
            A figure object created by plt.subplots.

        axarr: axis object
            An axis object created by plt.subplots.
        """
        if self.results:  # could be more fancy and checking results for meaningfulness, but this will do the job, too.
            for attr in set(self.__dict__.keys()).difference(['model', 'lfn', 'results']):
                delattr(self, attr)

            for attr in ('figure', '_axarr', 'axis'):
                setattr(self, attr, None)  # Making sure that all attributes created by Results.__init__ are present.

            self.__class__ = results.Results
            self.set_subplots(figure, axarr)  # cf. results.py

        else:
            raise ModelError("Simulate.results is empty!\nSimulation probably has not been run yet!")


class SteadyAnalysis(Simulate):
    """ set of methods to analyse the steady state behaviour"""
    # Fixme: Problem: SteadyStateLight needs a special light regime to yield meaningful results.
    # The methods want to scan through an array of different pfds with a constant light fn
    # best shot: create a step-function like light protocol and let the scan method increase the
    # counter each time a pfd in pfd_range is finished calculating.
    # Maybe note or warning that maxstep default for steady_state_light is supposed to be 1000
    # maybe note or warning that maxstep default for steady_state_light_scan is supposed to be 10000
    def __init__(self, model, lightprotocol, y0=None):
        raise NotImplementedError("SteadyAnalysis class methods are not yet implemented.")
        # Simulate.__init__(self, model, lightprotocol, y0)
        super().__init__(model, lightprotocol, y0)

        if y0 is None:
            self.y0 = np.array([1.70731726e+01, 3.71670630e-04, 5.00000000e+00, 3.33041952e-08,
                                0.00000000e+00, 2.50391755e-05, 9.24622663e-01, 6.22837684e-11])

    def steady_state_light(self, abstol=1e-3, t_step=1):
        """ returns the steady state values for given light intensity and for given tolerance error
            default y0 corresponds to the steady state in the darkness
        """
        raise NotImplementedError('The steady_state_light() method is not yet implemented.')

        self._successful = True
        y0s = [self.y0]
        self.integrator.set_initial_value(self.y0, 0)

        t, i = 0, 0
        y0 = self.y0
        err = np.linalg.norm(self.y0, ord=2)

        while self._successful and i < self.max_step and err > abstol:
            y0s = self.integrate(t + t_step)
            t += t_step
            i += 1
            err = np.linalg.norm(y0s - y0, ord=2)
            print('t=', t, ' err=', err)
            y0 = y0s

        if self.does_monitor() and self._successful:
            self.results.append({'t': np.array([t]), 'y': np.vstack([y0s]), 'lfn_counter': self.lfn.i})

        return y0s

    def steady_state_light_scan(self, pfd_range, abstol=1e-3, t_step=1):
        """ calculates steady state at time t for given list of PFDs """
        # code from lightmodel
        # ys = np.zeros(len(pfd_range), len(self.y0))
        # for i in range(len(self.y0)):
        #     pfd = pfd_range[i]
        #     ys[i, :] = self.steady_state_light(pfd, abstol, t_step, self.max_step)
        raise NotImplementedError('Method not yet ported from lightmodel (fixme note in lightmodel)')

        # old approach as method of Simulate
        # Ys = np.zeros((len(PFDrange), len(y0)))
        # T = np.linspace(0, t, t*10)  # time course method needs vector of time
        #
        # for cnt in range(len(PFDrange)):
        #     PFD = PFDrange[cnt]
        #     l = lightProtocol.LightProtocol({'protocol':'const','PFD':PFD})
        #
        #     Ys[cnt, :] = self.time_course(l, T, y0)[-1,:]
        #
        # return Ys

