#!/usr/bin/env python
# -*- coding: utf-8 -*-
# version: python 3.5

# TODO: Parameters.save()    Add option to add and store value comments.

"""
The mathematical model of the photosynthetic electron transport chain defines methods to calculate reaction rates
and set of ten differential equations based on the model published by Ebenhoeh et al. in 2014

Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.
"""

import csv
import numpy as np
from numpy.core.umath import log, exp

__author__ = ["Anna Matuszyńska", "Philipp Norf"]
__copyright__ = "Copyright 2016, Heinrich-Heine University Dusseldorf"
__credits__ = ["Anna Matuszynska", "Oliver Ebenhoeh", "Philipp Norf"]
__maintainer__ = "Philipp Norf"
__email__ = "philipp.norf@uni-duesseldorf.de"
__status__ = "Development"


class Parameters:
    """
    Class to manage parameters.
        * reads missing parameters from parameters.csv
        * calculates composite parameters ad hoc
        * calculates rate constants ad hoc
        * allows to store the current parameters in parameters.csv
    """
    def __init__(self, species='default', fallbacks=(), path='parameters.csv', failsave_mode=False, **parameter):
        self.species = species
        self.lookup_order = [self.species] + list(fallbacks) + ['default']
        self.path = path
        self.failsave_mode = failsave_mode
        self.custom_parameter = parameter
        self.parameter, self.composite_parameter, self.rateconstant = {}, {}, {}
        self.process()

    def __call__(self):
        """
        Creates the parameter dictionary ad hoc and returns it.
        This is used during self.__init__() and prevents excessive variable juggling.

        Returns
        -------
        dict
            An ad hoc created dictionary containing all parameters.
        """
        return {**self.parameter, **self.composite_parameter, **self.rateconstant}

    def __getattr__(self, attr):
        """
        Redirects to self(), to create the parameter dictionary ad hoc and returns the desired parameter.
        This is used during self.__init__() and prevents excessive variable juggling.

        Parameters
        ----------
        attr: str
            Name of a parameter.

        Returns
        -------
        float
            A single parameter.
        """
        return self()[attr]

    def __getitem__(self, key):
        """
        Enables subscripts to access parameters.

        Parameters
        ----------
        key: str
            Name of a parameter.

        Returns
        -------
        float
            A single parameter.
        """
        return getattr(self, key)

    def edit(self, recalculate=False, **parameter):
        """
        Allows to edit parameters such that they can be stored permanently (simply overwriting an attribute is only
        temporary and will not affect method save()).

        Parameters
        ----------
        parameter: **parameter
            Custom parameters as keyword arguments.

        recalculate: bool
            Flag. If set True, edit calls process afterwards. Defaults to false.
        """
        self.custom_parameter = {**self.custom_parameter, **parameter}
        if recalculate:
            self.process()

    def read(self, keys=None, path=None, failsave_mode=None):
        """
        Reads out parameters.csv and returns a dictionary with the keys, the parameter vectors and the column names.

        Parameters
        ----------
        keys: [str] or None
            List of the keys to look up.


        path: str or None
            Path to parameters.csv. Defaults to self.path if set None.

        failsave_mode: bool or None
            Flag. If set True a warning is printed to the screen and the method continues. If set False and exception is
            raised. If set to None, self.failsave_mode is used. Defaults to None.

        Returns
        -------
        dict
            A dictionary with the keys, the parameter vectors and the column names.

        """
        path = self.path if path is None else path
        failsave = self.failsave_mode if failsave_mode is None else failsave_mode
        out = {'species': []}
        with open(path, 'r') as parametercsv:
            reader = csv.reader(parametercsv, delimiter=';')

            # Extracting list of species from header row
            row = next(reader)
            for i in range(3, len(row), 2):
                out['species'].append((row[i], i))

            keys = keys if keys is not None else [spec for spec, index in out['species']]

            # Reacting to failure mode settings
            missing = set(keys).difference([spec for spec, index in out['species']])
            if missing and failsave:
                print("[warning]: Missing keys '{}' are skipped.".format("', '".join(missing)))
            elif missing:
                raise KeyError("File '{}' has no columns named '{}'!".format(path, "', '".join(missing)))

            # Getting column indices and reading species
            columns = {**{'key': 0}, **{spec: index for spec, index in out['species'] if spec in keys}}
            for key in columns:
                out[key] = []

            for row in reader:
                for key, index in columns.items():
                    out[key].append(row[index])

            # set missing values to None so they are distinguishable from 0
            for spec, index in out['species']:
                if spec in keys:
                    out[spec] = [float(v) if v != '' else None for v in out[spec]]

            out['species'] = [key for key, index in out['species']]
        return out

    def process(self, **parameter):
        """
        Processes the custom parameters, completes them according to the lookup order, and then calculates composite
        parameters and state variables. All parameters are stored in self.parameters and set as attributes for fast
        lookup.

        Note, that the parameter completion is actually a sequential reading and overwriting cycle of the reverse lookup
        order. This was easier to implement because this way omits extensive checking and re-reading of parameters.csv.

        Parameters
        ----------
        parameter: dict
            dictionary with parameters. Note, that unknown parameters are simply ignored.
        """
        self.edit(**parameter)
        # creates the parameter set recursively by loading and overwriting sets in reverse fallback order.
        read = self.read(self.lookup_order[::-1])

        for species in read['species']:
            if species in self.lookup_order:
                species_parameter = {k: v for k, v in zip(read['key'], read[species]) if v is not None}
                self.parameter = {**self.parameter, **species_parameter}

        self.parameter = {**self.parameter, **self.custom_parameter}
        self.composite_parameter['RT'] = self.R * self.T
        self.composite_parameter['dG_pH'] = log(10)*self.RT
        self.composite_parameter['Hstroma'] = 3.2e4*10**(-self.pHstroma)
        self.composite_parameter['kProtonation'] = 4e-3 / self.Hstroma  # [1/s] converted from 4 * 10^-6 [1/ms]
        self.rateconstant['KeqPQred'] = self.Keq_PQred()                # protonation of LHCs (L), depends on pH value
        self.rateconstant['KeqCyc'] = self.Keq_cyc()                    # in lumen
        self.rateconstant['KeqCytfPC'] = self.Keq_cytfPC()
        self.rateconstant['KeqFAFd'] = self.Keq_FAFd()
        self.rateconstant['KeqPCP700'] = self.Keq_PCP700()
        self.rateconstant['KeqNDH'] = self.Keq_NDH()
        self.rateconstant['KeqFNR'] = self.Keq_FNR()

        for k, v in self().items():
            setattr(self, k, v)

    def save(self, label=None, path=None, overwrite=False, auto_complete=False):
        """
        Saves the current parameters (but not composite parameters and state variables) into the parameters table.

        Parameters
        ----------
        label: str or None
            Column label for parameters.csv. Defaults to self.species if set None.

        path: str or None
            Path to parameters.csv. Defaults to self.path if set None.

        overwrite: bool
            Flag. If set True, existing columns will be overwritten. Defaults to False.

        auto_complete: bool
            Flag. If set True, self.parameters is written to parameters.csv, thus, missing parameters will be filled by
            the lookup chain. If set False, self.custom_parameters us used. Defaults to False.
        """
        label = self.species if label is None else label
        path = self.path if path is None else path

        read = self.read()
        values = [label]
        parameter = self.parameter if auto_complete else self.custom_parameter

        for key in read['key']:
            values.append(parameter[key] if key in parameter else '')
            # comments.append(comment[key] if key in comment else '')  # FIXME: Comments not yet implemented!

        with open(path, 'r') as parametercsv:
            file = np.array([row for row in csv.reader(parametercsv, delimiter=';')])

        if label in read['species']:
            column = np.where(file[0] == label)[0]
            if overwrite:
                rows = range(len(file))
            else:
                rows = np.where(file[:, column] == '')[0]
            file[rows, column] = np.transpose([values])[rows, 0]
            # file[rows, column + 1] = np.transpose([comments])[rows, 0]  # FIXME: not yet implemented!
        else:
            comments = [label + '_comment'] + [''] * (len(values) - 1)  # FIXME: dummy column because see above FIXME.
            file = np.hstack([file, np.transpose([values, comments])])

        with open(path, 'w') as output:
            writer = csv.writer(output, delimiter=';')
            for row in file:
                writer.writerow(row)

    # Rate constant formulas:
    def Keq_PQred(self):
        DG1 = -self.E0_QA * self.F
        DG2 = -2 * self.E0_PQ * self.F
        DG = -2 * DG1 + DG2 + 2 * self.pHstroma * self.dG_pH
        K = exp(-DG/self.RT)
        return K

    def Keq_cyc(self):
        DG1 = -self.E0_Fd * self.F
        DG2 = -2 * self.E0_PQ * self.F
        DG = -2 * DG1 + DG2 + 2 * self.dG_pH * self.pHstroma
        K = exp(-DG/self.RT)
        return K

    def Keq_cytfPC(self):
        DG1 = -self.E0_cytf * self.F
        DG2 = -self.E0_PC * self.F
        DG = -DG1 + DG2
        K = exp(-DG/self.RT)
        return K

    def Keq_FAFd(self):
        DG1 = -self.E0_FA * self.F
        DG2 = -self.E0_Fd * self.F
        DG = -DG1 + DG2
        K = exp(-DG/self.RT)
        return K

    def Keq_PCP700(self):
        DG1 = -self.E0_PC * self.F
        DG2 = -self.E0_P700 * self.F
        DG = -DG1 + DG2
        K = exp(-DG/self.RT)
        return K

    def Keq_NDH(self):
        DG1 = -2 * self.E0_NADP * self.F
        DG2 = -2 * self.E0_PQ * self.F
        DG = -DG1 + DG2 + self.dG_pH * self.pHstroma
        K = exp(-DG/self.RT)
        return K

    def Keq_FNR(self):
        DG1 = -self.E0_Fd * self.F
        DG2 = -2 * self.E0_NADP * self.F
        DG = -2 * DG1 + DG2 + self.dG_pH * self.pHstroma
        K = exp(-DG/self.RT)
        return K
