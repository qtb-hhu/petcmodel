# petcmodel
Mathematical model of the photosynthetic electron transport chain (minimalists approach)

PETCModel defines methods to calculate reaction rates and set of eight ODEs
LightModel defines methods to calculate reaction rates and set of nine ODEs
DiatomModel defines methods to calculate reaction rates and set of nine ODEs

based on the model published by Oliver Ebenhoeh et al. in 2014.
Python 2 implementation by Anna Matuszyńska.
Python 3 port and interactive API Philipp Norf.

## License
Copyright (C) 2014-2016  Anna Matuszyńska, Oliver Ebenhöh, Philipp Norf

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details. You should have received a copy of the GNU General Public License
along with this program (license.txt).  If not, see <http://www.gnu.org/licenses/>.


## Project structure

| Object        | Description                                                            | Path                |
| ------------- | ---------------------------------------------------------------------- | ------------------- |
| parameters    | parameters, optimized for Chlamydomonas reinhardtii                    | ./parameters.py     |
| model         | equations and helper functions                                         | ./model.py          |
| simulate      | simulates an experiment uses: model, parameter, lightprotocol          | ./simulate.py       |
| lightfunction | class to set the light behaviour for every integration step            | ./lightfunction.py  |
| lightprotocol | class to create fine tuned light protocols for experiments             | ./lightprotocol.py  |
| miscellaneous | model specific  helper functions and classes.                          | ./miscellaneous.py  |
| insilico_...  | Files that run experiments **in silico** are prefixed with *insilico_* | ./insilico_ ... .py |
| | | |
| externals        | collection of useful tools.                                   | ./externals/                    |
| duration         | class that allows to type in times in a human readable format | ./externals/duration.py         |
| sunrise_equation | python implementation of the sunrise equation                 | ./externals/sunrise_equation.py |
| colors           | color generators                                              | ./externals/colors.py           |
| words            | word generators                                               | ./externals/words.py            |
| debugging        | debugging helper functions                                    | ./externals/debugging.py        |
| norm_tools       | functions for normalisation and cropping operations           | ./externals/norm_tools.py       |
| | | |
| requirements | python packages requirements of the model    | ./requirements.txt |
| license      | GNU General Public License version 3 (GLPv3) | ./license.txt      |


## Installing Python and the requirements
The model runs on Python 3.5. Best practice is to use a virtual environment:

```
    user@host$ virtualenv -p python3 .env
    user@host$ source .env/bin/activate
```

The file requirements.txt lists all additional python packages required to run the model. They can be installed via pip:

```
    pip install -r requirements.txt
```

## Known bugs
* lightprotocol.py displays an error message for an invalid default parameter (irrelevant for the model) 
* lightfunction.py some of the experimental light functions are not working yet. Nevermind that, always use '_constant'
* Some of the older insilico_-scripts do not work anymore as the model was heavily refactored in the mean time.